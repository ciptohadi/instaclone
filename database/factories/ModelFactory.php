<?php
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;
    $gender = $faker->randomElement(['male', 'female']);
    $name = $faker->name($gender);
    
    return [
        'name' => $name,
        'username' => $faker->unique()->userName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = Hash::make('secret'),
        'gender' => $gender,
        'biography' => $faker->paragraph
    ];
});

$factory->define(App\Photo::class, function (Faker $faker) {
    $user_ids = App\User::pluck('id');
    $user_id = $user_ids->random();
    
    return [
        'description' => $faker->paragraph,
        'user_id' => $user_id,
        'file_path' => '/home/cipto/softdev/php/instaclone/storage/uploads/2017/10/23/php96ohjE.png'
    ];
});

$factory->define(\App\Comment::class, function ($faker) {
    $user_ids = App\User::pluck('id');
    $user_id = $user_ids->random();

    return [
        'content' => $faker->paragraph,
        'user_id' => $user_id,
    ];
});

$factory->define(\App\Like::class, function ($faker) {
    $user_ids = App\User::pluck('id');
    $user_id = $user_ids->random();
    
    return [
        'user_id' => $user_id,
    ];
});
