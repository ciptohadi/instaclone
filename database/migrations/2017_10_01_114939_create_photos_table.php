<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->string('file_path');
            $table->unsignedInteger('user_id');
            $table->timestamps(6);
            
            $table->index('user_id');
            // Create a foreign key constraint and cascade on delete.
            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('photos', function (Blueprint $table) {
            // Drop the foreign key first
            $table->dropForeign('photos_user_id_foreign');
            // Now drop the basic index
            $table->dropIndex('photos_user_id_index');
        });
        
        Schema::dropIfExists('photos');
    }
}
