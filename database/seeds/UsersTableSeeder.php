<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_nums =5;

        $users = factory('App\User', $user_nums)->create();
        $user_ids = $users->pluck('id')->toArray();
        // dd($user_ids);
        $users->each(function ($user) use ($user_nums, $user_ids) {
            shuffle($user_ids);
            $user_tofollow_ids = array_slice($user_ids, rand(0, $user_nums));
            foreach ($user_tofollow_ids as $id) {
                if ($user->id == $id) {
                    continue;
                }
                $user->followings()->attach($id);
            }
        });
    }
}
