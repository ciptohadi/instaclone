<?php

use Illuminate\Database\Seeder;

class PhotosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $photos = factory('App\Photo', 10)->create();

        $photos->each(
            function ($photo) {
                $photo->comments()->saveMany(
                    factory(App\Comment::class, rand(0, 5))->make(['photo_id' => $photo->id])
                );

                $photo->likes()->saveMany(
                    factory(App\Like::class, rand(0, 5))->make()
                );

                $photo->comments->each(function ($comment) use ($photo) {
                    $comment->likes()->saveMany(
                        factory(App\Like::class, rand(0, 5))->make()
                    );

                    $comment->comments()->saveMany(
                        factory(App\Comment::class, rand(0, 5))->make(['photo_id' => $photo->id])
                    );

                    $comment->comments->each(function ($comment) {
                        $comment->likes()->saveMany(
                            factory(App\Like::class, rand(0, 5))->make()
                        );
                    });
                });
            }
        );
    }
}
