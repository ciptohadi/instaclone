<?php

$router->get('/', function () {
    return config('app.info', 'App Info');
});

$router->get('/search', ['uses' => 'SearchController@index']);

$router->group(['prefix' => '/notifications', 'middleware' => 'auth'], function ($router) {
    $router->get('/', 'NotificationController@index');
    $router->get('/{id}', 'NotificationController@show');
    $router->post('/{id}/mark-as-read', 'NotificationController@markAsRead');
    $router->delete('/{id}/mark-as-unread', 'NotificationController@markAsUnread');
});

// User resource
// =================

$router->group(
    ['prefix' => '/users'],
    function ($router) {
        $router->get('/', 'UserController@index');
        $router->get('/{user}', ['uses' => 'UserController@show', 'as'=> 'users.show']);
        $router->post('/', 'UserController@store');

        $router->group(['middleware' => 'auth'], function ($router) {
            $router->patch('/{user}', 'UserController@update');
            $router->delete('/{user}', 'UserController@destroy');
        });
    }
);

// Account feature
$router->group(
    ['prefix' => '/account'],
    function ($router) {
        $router->post('/register', 'UserController@store');
        $router->post('/login', 'AccountController@login');
        $router->post('/reset-password', 'AccountController@reset_password');

        $router->group(['middleware' => 'auth'], function ($router) {
            $router->delete('/logout', 'AccountController@logout');
            $router->patch('/me', 'UserController@update');
            $router->patch('/me/update-avatar', 'AccountController@update_avatar');
            $router->get('/me', ['uses' => 'AccountController@show', 'as'=> 'account.me.show']);
            $router->get('/me/followers', 'FollowController@index_followers');
            $router->get('/me/followings', 'FollowController@index_followings');
        });
    }
);


// Follow feature
// =================
$router->group(
    ['prefix' => '/users/{user}'],
    function ($router) {
        $router->get('/followers', ['uses' => 'FollowController@index_followers', 'as' => 'users.followers.index']);
        $router->get('/followings', ['uses' => 'FollowController@index_followings', 'as' => 'users.followings.index']);

        $router->group(['middleware' => 'auth'], function ($router) {
            $router->post('/follow', 'FollowController@store');
            $router->delete('/unfollow', 'FollowController@destroy');
        });
    }
);

// Photo resource
// =================
$router->group(
    ['prefix' => '/photos'],
    function ($router) {
        $router->get('/', ['uses' => 'PhotoController@index', 'as' => 'photos.index']);
        $router->get('/{photo}', ['uses' => 'PhotoController@show', 'as' => 'photos.show']);

        $router->group(['middleware' => 'auth'], function ($router) {
            $router->post('/', 'PhotoController@store');
            $router->patch('/{photo}', 'PhotoController@update');
            $router->delete('/{photo}', 'PhotoController@destroy');
        });
    }
);

// Photos of a user
$router->get('/users/{user}/photos', ['uses' => 'PhotoController@index']);
$router->get('/account/me/photos', ['uses' => 'PhotoController@index', 'middleware' => 'auth']);
$router->get('/account/me/photos-timeline', ['uses' => 'PhotoController@index', 'middleware' => 'auth']);


// generate photo image src-url
$router->get('photos/{id}/src', ['as' => 'photo_preview', 'uses' => 'FileController@getImageSrc']);
// photo image src url sample implementation
$router->get('photos/{id}/preview', ['uses' => 'FileController@preview']);

// generate user avatar image src-url
$router->get('users/{id}/avatar-src', ['as' => 'avatar_preview', 'uses' => 'FileController@getImageSrc']);
// user avatar image src url sample implementation
$router->get('users/{id}/avatar-preview', [ 'uses' => 'FileController@preview']);


// Comment feature
// =================
$router->group(
    ['prefix' => '/photos/{photo}/comments'],
    function ($router) {
        $router->get('/', ['uses' => 'CommentController@index', 'as' => 'photos.comments.index']);
        $router->get('/{comment}', ['uses' => 'CommentController@show', 'as' => 'photos.comments.show']);

        //comments index of a comment
        $router->get('/{comment}/comments', ['uses' => 'CommentController@index', 'as' => 'photos.comments.comments.index']);
        
        //child_comments index of a comment
        $router->get('/{comment}/child-comments', ['uses' => 'CommentController@index', 'as' => 'photos.comments.child_comments.index']);

        $router->group(['middleware' => 'auth'], function ($router) {
            $router->post('/', 'CommentController@store');
            $router->patch('/{comment}', 'CommentController@update');
            $router->delete('/{comment}', 'CommentController@destroy');

            //create a comment of a comment
            $router->post('/{comment}/comments/', 'CommentController@store');
        });
    }
);

// Like feature
// =================
$router->group(
    ['prefix' => '/photos/{photo}'],
    function ($router) {
        // likes of a photo
        $router->get('/likes', ['uses' => 'LikeController@index', 'as' => 'photos.likes.index']);
        $router->get('/likes/{like}', 'LikeController@show');
        
        // likes of a comment
        $router->get('/comments/{comment}/likes', ['uses' => 'LikeController@index', 'as' => 'photos.comments.likes.index']);
        $router->get('/comments/{comment}/likes/{like}', 'LikeController@show');

        // guarded with auth
        $router->group(['middleware' => 'auth'], function ($router) {
            // likes of a photo
            $router->post('/likes', 'LikeController@store');
            $router->delete('/likes/{like}', 'LikeController@destroy');

            // likes of a comment
            $router->post('/comments/{comment}/likes', 'LikeController@store');
            $router->delete('/comments/{comment}/likes/{like}', 'LikeController@destroy');
        });
    }
);
