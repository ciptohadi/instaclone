<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY', 'SomeRandomString!!!'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */
    'locale' => env('APP_LOCALE', 'en'),
    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */
    'fallback_locale' => env('APP_FALLBACK_LOCALE', 'en'),

    'info' =>  [
            'app' => [
                'name' => env('APP_NAME', 'Lumen App'),
                'description' => 'An awesome simple Instagram clone',
                'api_routes' => 'https://drive.google.com/file/d/0BwwMwCuweRO_aWNzcFVuM3haREk/view?usp=sharing'
            ],
            'developer' => [
                'name' => 'Cipto Hadi',
                'github' => 'https://github.com/ciptohadi-web-id',
                'gitlab' => 'https://gitlab.com/ciptohadi',
                'bitbucket' => 'https://bitbucket.org/ciptohadi',
            ]
        ]

];
