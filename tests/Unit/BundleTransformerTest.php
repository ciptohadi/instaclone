<?php

namespace Tests\Unit;

use Tests\DBTestCase;
use App\Bundle;
use App\Transformers\BundleTransformer;
use League\Fractal\TransformerAbstract;

class BundleTransformerTest extends DBTestCase
{
    protected $subject;
    
    public function setUp(){
        parent::setUp();
        $this->subject = new BundleTransformer();
    }
    
    /** @test **/
    public function it_can_be_initialized()
    {
        $this->assertInstanceOf(TransformerAbstract::class, $this->subject);
    }
    
    /** @test **/
    public function it_transforms_a_bundle_model()
    {
        $bundle = create(Bundle::class);
        $transform = $this->subject->transform($bundle); 
        
        $this->assertEquals($bundle->id, $transform['id']);
        $this->assertEquals($bundle->title, $transform['title']);
        $this->assertEquals($bundle->description, $transform['description']);
        $this->assertEquals($bundle->created_at->toIso8601String(), $transform['created']);
        $this->assertEquals($bundle->updated_at->toIso8601String(), $transform['updated']);
        
        
    }
    
    /** @test **/
    public function it_can_transform_related_books()
    {
        $bundle = $this->bundleFactory();
        
        $data = $this->subject->includeBooks($bundle);
        $this->assertInstanceOf(\League\Fractal\Resource\Collection::class, $data);
        $this->assertInstanceOf(\App\Book::class, $data->getData()[0]);
        $this->assertCount(2, $data->getData());
    }
    
   
}
