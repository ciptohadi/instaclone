<?php

namespace Tests\Unit;

use Tests\DBTestCase;
use Laravel\Lumen\Testing\DatabaseMigrations;

class AuthorsRatingsControllerTest extends DBTestCase
{

    /** @test **/
    public function store_can_add_a_rating_to_an_author()
    {
        $author = create('App\User', ['is_author' => true]);

        $this->post(
            "/authors/{$author->id}/ratings",
            ['value' => 5],
            ['Accept' => 'application/json']
        );

        $this
            ->seeStatusCode(201)
            ->seeJson([
                'value' => 5
            ])
            ->seeJson([
                'rel' => 'user',
                'href' => route('authors.show', ['id' => $author->id])
            ]);

        $body = $this->response->getData(true);
        $this->assertArrayHasKey('data', $body);

        $data = $body['data'];
        $this->assertArrayHasKey('links', $data);
    }

    /** @test **/
    public function store_fails_when_the_author_is_invalid()
    {
        
        $this->post('/authors/3/ratings', ['value' => 5], ['Accept' => 'application/json']);
        $this->seeStatusCode(404);
    }

    /** @test **/
    public function destroy_can_delete_an_author_rating()
    {
        $author = create('App\User', ['is_author' => true]);
        
        $ratings = $author->ratings()->saveMany(
            factory(\App\Rating::class, 2)->make()
        );

        $this->assertCount(2, $ratings);

        $ratings->each(function (\App\Rating $rating) use ($author) {
            $this->seeInDatabase('ratings', [
                'rateable_id' => $author->id,
                'id' => $rating->id
            ]);
        });

        $ratingToDelete = $ratings->first();
        $this
            ->delete(
                "/authors/{$author->id}/ratings/{$ratingToDelete->id}"
            , [], ['Accept' => 'application/json'])
            ->seeStatusCode(204);

        $dbAuthor = \App\User::find($author->id);
        $this->assertCount(1, $dbAuthor->ratings);
        $this->notSeeInDatabase(
            'ratings',
            ['id' => $ratingToDelete->id]
        );
    }

    /** @test **/
    public function destroy_should_not_delete_ratings_from_another_author()
    {
        $authors = create('App\User', ['is_author' => true], 2);
        
        $authors->each(function (\App\User $author) {
            $author->ratings()->saveMany(
                factory(\App\Rating::class, 2)->make()
            );
        });

        $firstAuthor = $authors->first();
        $rating = $authors
            ->last()
            ->ratings()
            ->first();

        $this->delete("/authors/{$firstAuthor->id}/ratings/{$rating->id}", [])->seeStatusCode(404);
    }

    /** @test **/
    public function destroy_fails_when_the_author_is_invalid()
    {
        $this->delete('/authors/1/ratings/1', [], ['Accept' => 'application/json'])->seeStatusCode(404);
    }
}
