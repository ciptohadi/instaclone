<?php

namespace Tests\Unit;

use Tests\DBTestCase;
use Carbon\Carbon;

class BooksTest extends DBTestCase
{
  
    public function setUp()
    {
        parent ::setUp();
        // make Carbon::now() same as book create_at and update_at 
        Carbon::setTestNow(Carbon::now('UTC'));
    }
    
    public function tearDown()
    {
        parent ::tearDown();
        Carbon::setTestNow();
    }
    
    /** @test  */
    public function store_should_respond_with_a_201_and_location_header_when_successful()
    {
        // $this->signIn();

        $book =  make('App\Book');
        
        $this->post('/books', $book->toArray());
        
        // $this->seeStatusCode(201)
        //     ->seeJson(['created' => true])
        //     ->seeHeaderWithRegExp('Location', '#/books/[a-z0-9]+(?:-[a-z0-9]+)*$#');
        
        // $body = json_decode($this->response->getContent(), true );
        // $this->assertArrayHasKey('data', $body);
        // $data = $body['data'];
        // $this->assertEquals($data['title'], $book->title);
        // $this->assertEquals($data['description'], $book->description); 
        // $this->assertEquals($data['user_id'], $book->user_id);
        // $this->seeInDatabase('books', ['title' => $book->title]);
        
        
        $this->seeJson([
                'slug' => $book->slug,
                'title' => $book->title,
                'description' => $book->description,
                'author' => $book->author->name,
            ])
            ->seeInDatabase('books', ['title' => $book->title]);
            
        $content = json_decode($this->response->getContent(), true );
        $this->assertArrayHasKey('data', $content);
        $data = $content['data'];
            
        $this->assertArrayHasKey('created', $data);
        $this->assertEquals(Carbon::now()->toIso8601String(), $data['created']);
        $this->assertArrayHasKey('updated', $data);
        $this->assertEquals(Carbon::now()->toIso8601String(), $data['updated']);
    }
    
    /** @test **/
    public function update_should_only_change_fillable_fields()
    {
         $bookOne = create('App\Book'); 
         $bookTwoRaw = make('App\Book');
         
         $data = [
                'title' => $bookTwoRaw->title,
                'slug' => $bookTwoRaw->slug,
                'description' => $bookTwoRaw->description
            ];
         
        $this->notSeeInDatabase('books', [
            'title' => $bookTwoRaw->title,
        ]);

        $this->put($bookOne->path(), $data, ['Accept' => 'application/json']);

        $this->seeStatusCode(200)
            ->seeJson($data)
            ->seeInDatabase('books', [
                'title' => $bookTwoRaw->title
            ]);
        
        $body = json_decode($this->response->getContent(), true );
        $this->assertArrayHasKey('data', $body);
        
        $data = $body['data'];
        $this->assertArrayHasKey('created', $data);
        $this->assertEquals(Carbon::now()->toIso8601String(), $data['created']);
        $this->assertArrayHasKey('updated', $data);
        $this->assertEquals(Carbon::now()->toIso8601String(), $data['updated']);
    }
    /** @test **/
    public function update_should_fail_with_an_invalid_id()
    {
        $this->put('/books/invalid-id')
            ->seeStatusCode(404)
            ->seeJsonEquals([
                'error' => [
                    'message' => 'Book not found'
                ]
            ]);
    }
    /** @test **/
    public function update_should_not_match_an_invalid_route()
    {
        $this->put('/books/-invalid-route-')
            ->seeStatusCode(400)
            ->seeJsonEquals([
                'error' => [
                    'message' => 'Invalid slug'
                ]
            ]);
    }

     /** @test **/
    public function destroy_should_remove_a_valid_book()
    {
        $book = create('App\Book');
        $this
            ->delete($book->path())
            ->seeStatusCode(204)
            ->isEmpty();

        $this->notSeeInDatabase('books', ['id' => $book->id]);
    }

    /** @test **/
    public function destroy_should_return_a_404_with_an_invalid_id()
    {
        $this
            ->delete('/books/invalid-id')
            ->seeStatusCode(404)
            ->seeJsonEquals([
                'error' => [
                    'message' => 'Book not found'
                ]
            ]);
    }

    /** @test **/
    public function destroy_should_not_match_an_invalid_route()
    {
        $this->delete('/books/-invalid-route-')
             ->seeStatusCode(400)
            ->seeJsonEquals([
                'error' => [
                    'message' => 'Invalid slug'
                ]
            ]);
    }
}
