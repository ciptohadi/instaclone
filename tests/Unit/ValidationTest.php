<?php
namespace Tests\Unit;

use Tests\DBTestCase;
use Illuminate\Http\Response;

class ValidationTest extends DBTestCase
{
    /** @test **/
    public function it_validates_required_fields_when_creating_a_new_book()
    {
        $this->post('/books', [], ['Accept' => 'application/json']);
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $this->response->getStatusCode());
        
        $body = json_decode($this->response->getContent(), true );
        $this->assertArrayHasKey('title', $body);
        $this->assertArrayHasKey('slug', $body);
        $this->assertArrayHasKey('description', $body);
        $this->assertArrayHasKey('user_id', $body);
        
        $this->assertEquals(["The title field is required."], $body['title']);
        $this->assertEquals(["The slug field is required."], $body['slug']);
        $this->assertEquals(["Please provide a description."], $body['description']);
        $this->assertEquals(["The user id field is required."], $body['user_id']);
    }
    
    /** @test **/
    public function it_validates_requied_fields_when_updating_a_book()
    {
        $book = create(\App\Book::class);
        
        $this->put($book->path(), [], ['Accept' => 'application/json']);
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $this->response->getStatusCode());
        
        $body = json_decode($this->response->getContent(), true );
        $this->assertArrayHasKey('title', $body);
        $this->assertArrayHasKey('slug', $body);
        $this->assertArrayHasKey('description', $body);
        
        $this->assertEquals(["The title field is required."], $body['title']);
        $this->assertEquals(["The slug field is required."], $body['slug']);
        $this->assertEquals(["Please provide a description."], $body['description']);
    }
    
    /** @test  */
    public function title_fails_create_validation_when_just_too_long()
    {
        // $this->signIn();

        $book =  make('App\Book', ['title' => str_repeat('a', 256)]);
        
        $this->post('/books', $book->toArray());
        
        $this->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJson([
                    'title' => ["The title may not be greater than 255 characters."]
                ])
            ->notSeeInDatabase('books', ['title' => $book->title]);
    }
    
    /** @test **/
    public function title_fails_update_validation_when_just_too_long()
    {
        $bookOne = create('App\Book'); 
        $bookTwoRaw = make('App\Book', ['title' => str_repeat('a', 256)]);
         
          // $this->signIn();
        
        $this->put($bookOne->path(), $bookTwoRaw->toArray());
        
        $this->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJson([
                    'title' => ["The title may not be greater than 255 characters."]
                ])
            ->notSeeInDatabase('books', ['title' => $bookTwoRaw->title]);
    }
    
    /** @test  */
    public function title_passes_create_validation_when_exactly_max()
    {
        // $this->signIn();

        $book =  make('App\Book', ['title' => str_repeat('a', 255)]);
        
        $this->post('/books', $book->toArray());
        
        $this->seeStatusCode(Response::HTTP_CREATED)
            ->seeInDatabase('books', ['title' => $book->title]);
    }
    
    /** @test **/
    public function title_passes_update_validation_when_exactly_max()
    {
        $bookOne = create('App\Book'); 
        $bookTwoRaw = make('App\Book', ['title' => str_repeat('a', 255)]);
         
          // $this->signIn();
        
        $this->put($bookOne->path(), $bookTwoRaw->toArray());
        
        $this->seeStatusCode(Response::HTTP_OK)
            ->seeInDatabase('books', ['title' => $bookTwoRaw->title]);
    }
}