<?php

namespace Tests\Unit;

use Tests\DBTestCase;
use App\User;
use App\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;

class UserTransformerTest extends DBTestCase
{
    protected $subject;
    
    public function setUp(){
        parent::setUp();
        $this->subject = new UserTransformer();
    }
    
    /** @test **/
    public function it_can_be_initialized()
    {
        $this->assertInstanceOf(TransformerAbstract::class, $this->subject);
    }
    
    /** @test **/
    public function it_transforms_a_User_model()
    {
        $author = create(User::class, ['is_author' => true]);
        
        $author->ratings()->save(
            factory(\App\Rating::class)->make(['value' => 5])
        );
        $author->ratings()->save(
            factory(\App\Rating::class)->make(['value' => 3])
        );
        
        $transform = $this->subject->transform($author); 
        
        $this->assertEquals($author->name, $transform['name']);
        $this->assertEquals($author->email, $transform['email']);
        $this->assertEquals($author->gender, $transform['gender']);
        $this->assertEquals($author->biography, $transform['biography']);
        $this->assertEquals($author->created_at->toIso8601String(), $transform['created']);
        $this->assertEquals($author->updated_at->toIso8601String(), $transform['updated']);
        
         // Rating
        $this->assertArrayHasKey('rating', $transform);
        $this->assertInternalType('array', $transform['rating']);
        $this->assertEquals(4, $transform['rating']['average']);
        $this->assertEquals(5, $transform['rating']['max']);
        $this->assertEquals(80, $transform['rating']['percent']);
        $this->assertEquals(2, $transform['rating']['count']);
    }
    
    /** @test **/
    public function it_can_transform_related_books()
    {
        $book = create('App\Book');
        $author = $book->author;

        $data = $this->subject->includeBooks($author);
        $this->assertInstanceOf(\League\Fractal\Resource\Collection::class, $data);
    }
}
