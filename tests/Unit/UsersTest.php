<?php

namespace Tests\Unit;

use Tests\DBTestCase;
use Carbon\Carbon;
use Illuminate\Http\Response;

class UsersTest extends DBTestCase
{
  
    public function setUp()
    {
        parent ::setUp();
        // make Carbon::now() same as user create_at and update_at 
        Carbon::setTestNow(Carbon::now('UTC'));
    }
    
    public function tearDown()
    {
        parent ::tearDown();
        Carbon::setTestNow();
    }
    
    /** @test  */
    public function store_should_respond_with_a_201_and_location_header_when_successful()
    {
        // $this->signIn();

        $user =  make('App\User', ['is_author' => true]);
        
        $this->post('/authors', $user->makeVisible('password')->toArray());
        
        $this->seeStatusCode(201)
            ->seeJson([
                'name' => $user->name,
                'email' => $user->email,
                'gender' => $user->gender,
                'biography' => $user->biography,
            ])
            ->seeInDatabase('users', ['name' => $user->name]);
            
        $content = json_decode($this->response->getContent(), true );
        $this->assertArrayHasKey('data', $content);
        $data = $content['data'];
            
        $this->assertArrayHasKey('created', $data);
        $this->assertEquals(Carbon::now()->toIso8601String(), $data['created']);
        $this->assertArrayHasKey('updated', $data);
        $this->assertEquals(Carbon::now()->toIso8601String(), $data['updated']);
    }
    
    /** @test **/
    public function update_should_only_change_fillable_fields()
    {
        $userOne = create('App\User', ['is_author' => true]); 
        $userTwoRaw = make('App\User', ['is_author' => true]);
      
        $this->notSeeInDatabase('users', [
            'name' => $userTwoRaw->name,
        ]);

        $data = $userTwoRaw->makeVisible('password')->toArray();
        
        $this->put($userOne->path(), $data, ['Accept' => 'application/json']);

        unset($data['password'], $data['is_author']);
        
        $this->seeStatusCode(200)
            ->seeJson($data)
            ->seeInDatabase('users', $data);
        
        $body = json_decode($this->response->getContent(), true );
        $this->assertArrayHasKey('data', $body);
        
        $data = $body['data'];
        $this->assertArrayHasKey('created', $data);
        $this->assertEquals(Carbon::now()->toIso8601String(), $data['created']);
        $this->assertArrayHasKey('updated', $data);
        $this->assertEquals(Carbon::now()->toIso8601String(), $data['updated']);
    }
    /** @test **/
    public function update_should_fail_with_an_invalid_id()
    {
        // no author, havent't created
        $this->put('/authors/1')
            ->seeStatusCode(404)
            ->seeJsonEquals([
                'error' => [
                    'message' => 'User not found or Not an author'
                ]
            ]);
    }
    /** @test **/
    public function update_should_not_match_an_invalid_route()
    {
        $this->put('/users/-invalid-route-')
            ->seeStatusCode(404)
             ->assertNotRegExp(
                        '/User not found or Not an author/',
                        $this->response->getContent(),
                        'UserController@update route matching when it should not.'
                    );
    }

     /** @test **/
    public function destroy_should_remove_a_valid_user()
    {
        $user = create('App\User', ['is_author' => true]);
        $this
            ->delete($user->path())
            ->seeStatusCode(204)
            ->isEmpty();

        $this->notSeeInDatabase('users', ['id' => $user->id])
            ->notSeeInDatabase('books', ['author_id' => $user->id]);;
    }

    /** @test **/
    public function destroy_should_return_a_404_with_an_invalid_id()
    {
        // no user created yet
        $this
            ->delete('/authors/1')
            ->seeStatusCode(404)
            ->seeJsonEquals([
                'error' => [
                    'message' => 'User not found or Not an author'
                ]
            ]);
    }

    /** @test **/
    public function destroy_should_not_match_an_invalid_route()
    {
        $this->delete('/authors/-invalid-route-')
             ->seeStatusCode(404)
            ->assertNotRegExp(
                        '/User not found or Not an author/',
                        $this->response->getContent(),
                        'UserController@destroy route matching when it should not.'
                    );
    }
    
    /* VALIDATION */
    // ====================
    
    /** @test **/
    public function validation_validates_required_fields()
    {
        $test_data = $this->getCreateUpdateValidationTestData();
        
        foreach($test_data as $test_datum) {
            $method = $test_datum['method'];
            $url = $test_datum['url'];
            
            $this->{$method}($url, [], ['Accept' => 'application/json']);
            
            $response_data = $this->response->getData( true );
            
            $fields = ['name', 'email', 'password', 'gender', 'biography'];
            
            foreach ($fields as $field) {
                $this->assertArrayHasKey($field, $response_data);
                $this->assertEquals(["The {$field} field is required."], $response_data[$field]);
            }
        }
    }
    
    /** @test **/
    public function validation_invalidates_incorrect_gender_data()
    {
        $test_data = $this->getCreateUpdateValidationTestData();
        
        foreach($test_data as $test_datum) {
            $method = $test_datum['method'];
            $url = $test_datum['url'];
            $sent_data = $test_datum['data'];
            $sent_data['gender'] = 'invalid_gender';
            
            $this->{$method}($url, $sent_data, ['Accept' => 'application/json']);
            
            $this->seeStatusCode(422);
            $data = $this->response->getData( true );
            $this->assertCount(1, $data);
            $this->assertArrayHasKey('gender', $data);
            $this->assertEquals(
                ["Gender format is invalid: must equal 'male' or 'female'"],
                $data['gender']
            );
        }
    }
    
    /** @test **/
    public function name_fails_validation_when_just_too_long()
    {
        $test_data = $this->getCreateUpdateValidationTestData();
        
        foreach($test_data as $test_datum) {
            $method = $test_datum['method'];
            $url = $test_datum['url'];
            $sent_data = $test_datum['data'];
            $sent_data['name'] = str_repeat('a', 256);
            
            $this->{$method}($url, $sent_data, ['Accept' => 'application/json']);
            
            
            $this->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
                ->seeJson([
                        'name' => ["The name may not be greater than 255 characters."]
                    ])
                ->notSeeInDatabase('users', ['name' => $sent_data['name']]);
        }
    }
   
   /** @test  */
    public function name_passes_validation_when_exactly_max()
    {
        // $this->signIn();
        
        $test_data = $this->getCreateUpdateValidationTestData();
       
        foreach($test_data as $test_datum) {
            $method = $test_datum['method'];
            $url = $test_datum['url'];
            $sent_data = $test_datum['data']; 
            $sent_data['name'] = str_repeat('a', 255); 
          
            $this->{$method}($url, $sent_data, ['Accept' => 'application/json']);
            
            unset($sent_data['password'], $sent_data['is_author']); // unset for assertion using seeJson
            
            $this->seeStatusCode($test_datum['status'])
                ->seeJson($sent_data)
                ->seeInDatabase('users', ['name' => $sent_data['name']]);
        }
        
        
    }
    
    
    /**
     * Provides boilerplate test instructions for validation.
     * @return array
     */
    private function getCreateUpdateValidationTestData()
    {
        $userOne = create('App\User', ['is_author' => true]); 
        $userTwoRaw = make('App\User', ['is_author' => true]);
        $userThreeRaw = make('App\User', ['is_author' => true]);
        
        $data_post = $userTwoRaw->makeVisible('password')->toArray();
        $data_put = $userThreeRaw->makeVisible('password')->toArray();
        
        return [
            // Create
            [
                'method' => 'post',
                'url' => '/authors',
                'status' => Response::HTTP_CREATED,
                'data' => $data_post
            ],

            // Update
            [
                'method' => 'put',
                'url' => $userOne->path(),
                'status' => Response::HTTP_OK,
                'data' => $data_put
            ]
        ];
    }
    
     /** @test **/
    public function store_returns_a_valid_location_header()
    {
        $postData = [
            'name' => 'H. G. Wells',
            'gender' => 'male',
            'email' => 'me@mail.co',
            'password' => 'secret',
            'biography' => 'Prolific Science-Fiction Writer'
        ];
     
        $this
            ->post('/authors', $postData,
                ['Accept' => 'application/json'])
            ->seeStatusCode(201);

        $data = $this->response->getData(true);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('id', $data['data']);

        // Check the Location header
        $id = $data['data']['id'];
        $this->seeHeaderWithRegExp('Location', "#/authors/{$id}$#");
    }
    
}
