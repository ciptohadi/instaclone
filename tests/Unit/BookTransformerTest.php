<?php

namespace Tests\Unit;

use Tests\DBTestCase;
use App\Book;
use App\Transformers\BookTransformer;
use League\Fractal\TransformerAbstract;

class BookTransformerTest extends DBTestCase
{
    protected $subject;
    
    public function setUp() {
        parent::setUp();
        $this->subject = new BookTransformer();
    }
    
    /** @test **/
    public function it_can_be_initialized()
    {
        $this->assertInstanceOf(TransformerAbstract::class, $this->subject);
    }
    
    /** @test **/
    public function it_transforms_a_book_model()
    {
        $book = create(Book::class);
        $subject = new BookTransformer();
        $transform = $subject->transform($book);
        $this->assertArrayHasKey('id', $transform);
        $this->assertArrayHasKey('title', $transform);
        $this->assertArrayHasKey('description', $transform);
        $this->assertArrayHasKey('author', $transform);
        $this->assertArrayHasKey('created', $transform);
        $this->assertArrayHasKey('updated', $transform);
    }
    
    /** @test **/
    public function it_can_transform_related_author()
    {
        $authors = create('App\User', [], 3);
        $books = create('App\Book', [], 10);
        
        foreach ($books as $book) {
            $author = $book->author;
            $data = $this->subject->includeAuthor($book);
            $this->assertInstanceOf(\League\Fractal\Resource\Item::class, $data);
        }
    }
}
