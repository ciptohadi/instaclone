<?php

namespace Tests\Feature;

use Tests\DBTestCase;

class CreateUsersTest extends DBTestCase
{
   
    /** @test  */
    public function an_authenticated_user_may_create_authors()
    {
        // $this->signIn();

        $user =  make('App\User', ['is_author' => true]);
        
        $this->post('/authors', $user->makeVisible('password')->toArray());
        
        $content = json_decode($this->response->getContent(), true ); 
        $this->assertArrayHasKey('data', $content);
        
        $this->seeStatusCode(201)
            ->seeJson([
                'name' => $user->name,
                'email' => $user->email,
                'gender' => $user->gender,
                'biography' => $user->biography,
            ])
            ->seeInDatabase('users', ['name' => $user->name]);
    }
}
