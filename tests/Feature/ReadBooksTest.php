<?php

namespace Tests\Feature;

use Tests\DBTestCase;

class ReadBooksTest extends DBTestCase
{
    protected $books;
    
    public function setUp()
    {
        parent::setUp();
        
        $this->books = create('App\Book', [], 3);
    }
    
    /** @test */
    public function a_user_can_view_books_index()
    {
        // $response = $this->get('/books');

        // foreach ($this->books as $book) {
        //     $response->seeJson([
        //             'title' => $book->title
        //         ]);
        // }
       
        // repond with array 
        // $expected = ['data' => $this->books->toArray()];
       
        // $this->get('/books')
        //     ->seeJsonEquals($expected);
    
        $this->get('/books');
        $content = json_decode($this->response->getContent(), true );
        $this->assertArrayHasKey('data', $content);
        
        foreach ($this->books as $book) {
            $this->seeJson([
                'id' => $book->id,
                'slug' => $book->slug,
                'title' => $book->title,
                'description' => $book->description,
                'author' => $book->author->name,
                'created' => $book->created_at->toIso8601String(),
                'updated' => $book->updated_at->toIso8601String(),
            ]);
        }
        
    }
    
     /** @test  */
    public function a_user_can_view_a_single_valid_book()
    {
        $book =  $this->books->first();
        
        // $this->get($book->path())
        //         ->seeStatusCode(200)
        //         ->seeJson([
        //             'title' => $book->title
        //         ]);
                
        
        // $data = json_decode($this->response->getContent(), true );
        // $this->assertArrayHasKey('created_at', $data);
        // $this->assertArrayHasKey('updated_at', $data);
        
        // $expected = ['data' => $book->toArray()];
        
        // $this->get($book->path())
        //     ->seeStatusCode(200)
        //     ->seeJsonEquals($expected);
        
        $this->get($book->path());
        $content = json_decode($this->response->getContent(), true );
        $this->assertArrayHasKey('data', $content);
        
        $this->seeJson([
                'id' => $book->id,
                'slug' => $book->slug,
                'title' => $book->title,
                'description' => $book->description,
                'author' => $book->author->name,
                'created' => $book->created_at->toIso8601String(),
                'updated' => $book->updated_at->toIso8601String(),
            ]);
    }
    
    /** @test */
    public function a_user_should_see_not_found_message_when_book_doesnt_exist()
    {
        $this->get('books/fail')
            ->seeStatusCode(404)
            ->seeJson([
                    'error' => [
                        'message' => 'Book not found'
                ]
                ]);
        
    }
    
    /** @test */
    public function a_user_should_see_invalid_slug_when_slug_isnt_well_formatted()
    {
        $invalid_slugs = ['-fail', 'fail-'];
        
        foreach ($invalid_slugs as $slug) {
            $this->get('books/' . $slug)
                ->seeStatusCode(400)
                ->seeJson([
                        'error' => [
                            'message' => 'Invalid slug'
                        ]
                    ]);
        }
        
        
    }
}
