<?php

namespace Tests\Feature;

use Tests\DBTestCase;

class CreateBooksTest extends DBTestCase
{
   
    /** @test  */
    public function an_authenticated_user_may_create_posts()
    {
        // $this->signIn();

        $book =  make('App\Book');
        
        $this->post('/books', $book->toArray());
        
        // $this->seeJson($book->toArray())
        //     ->seeInDatabase('books', ['title' => $book->title]);
            
        // $body = json_decode($this->response->getContent(), true );
        // $this->assertArrayHasKey('data', $body);
        
        // $this->get($book->path()); 
        $content = json_decode($this->response->getContent(), true );
        $this->assertArrayHasKey('data', $content);
        
        $this->seeJson([
                'slug' => $book->slug,
                'title' => $book->title,
                'description' => $book->description,
                'author' => $book->author->name,
            ])
            ->seeInDatabase('books', ['title' => $book->title]);
    }
}
