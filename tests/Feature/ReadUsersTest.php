<?php

namespace Tests\Feature;

use Tests\DBTestCase;
use Illuminate\Http\Response;

class ReadUsersTest extends DBTestCase
{
    protected $users;
    
    public function setUp()
    {
        parent::setUp();
        
        $this->users = create('App\User', ['is_author' => true], 3);
    }
    
     /** @test **/
    public function index_responds_with_200_status_code()
    {
        $this->get('/authors')->seeStatusCode(Response::HTTP_OK);
    }
    
    /** @test */
    public function a_user_can_view_users_index()
    {
        $this->get('/authors');
        
        foreach ($this->users as $user) {
            $this->seeJson([
                'name' => $user->name,
                'email' => $user->email,
                'gender' => $user->gender,
                'biography' => $user->biography,
                'created' => $user->created_at->toIso8601String(),
                'updated' => $user->updated_at->toIso8601String(),
            ]);
        }
        
        
        $content = json_decode($this->response->getContent(), true );
        $this->assertArrayHasKey('data', $content);
        
    }
    
     /** @test  */
    public function a_user_can_view_a_single_valid_user()
    {
        $user =  $this->users->first();
        $this->get($user->path());
        $content = json_decode($this->response->getContent(), true );
        $this->assertArrayHasKey('data', $content);
        
        $this->seeJson([
                'name' => $user->name,
                'email' => $user->email,
                'gender' => $user->gender,
                'biography' => $user->biography,
                'created' => $user->created_at->toIso8601String(),
                'updated' => $user->updated_at->toIso8601String(),
            ]);
    }
    
    /** @test */
    public function a_user_should_see_not_found_message_when_user_doesnt_exist()
    {
        $this->get('authors/'. ($this->users->count()+1))
            ->seeStatusCode(Response::HTTP_NOT_FOUND)
            ->seeJson( [
                        'message' => 'User not found or Not an author'
                    ]);
        
        $body = json_decode($this->response->getContent(), true );
        $this->assertArrayHasKey('error', $body);
        $error = $body['error'];
        $this->assertEquals('User not found or Not an author', $error['message']);
    }
    
    /** @test */
    public function show_route_shouldnot_match_invalid_id_ie_nondigit()
    {
        $invalid_slugs = ['-fail', 'fail-'];
        
        foreach ($invalid_slugs as $slug) {
            $this->get('authors/' . $slug)
                ->seeStatusCode(Response::HTTP_NOT_FOUND) // error because invalid id
                ->assertNotRegExp(
                        '/User not found or Not an author/',
                        $this->response->getContent(),
                        'UserController@show route matching when it should not.'
                    );
        }
    }
    
    /** @test **/
    public function show_optionally_includes_books()
    {
        $book = create('App\Book');
        $author = $book->author;
     
        $this->get(
            "/authors/{$author->id}?include=books",
            ['Accept' => 'application/json']
        );

        $body = json_decode($this->response->getContent(), true);

        $this->assertArrayHasKey('data', $body);
        $data = $body['data'];
        $this->assertArrayHasKey('books', $data);
        $this->assertArrayHasKey('data', $data['books']);
        $this->assertCount(1, $data['books']['data']);

        // See Author Data
        $this->seeJson([
            'name' => $author->name,
            'email' => $author->email,
        ]);

        // Test included book Data (the first record)
        $actual = $data['books']['data'][0];
        $this->assertEquals($book->id, $actual['id']);
        $this->assertEquals($book->title, $actual['title']);
        $this->assertEquals($book->description, $actual['description']);
        $this->assertEquals(
            $book->created_at->toIso8601String(),
            $actual['created']
        );
        $this->assertEquals(
            $book->updated_at->toIso8601String(),
            $actual['updated']
        );
    }

}
