<?php

namespace Tests;

use Laravel\Lumen\Testing\DatabaseMigrations;
// use Laravel\Lumen\Testing\DatabaseTransactions;

abstract class DBTestCase extends TestCase
{
    use DatabaseMigrations;
    
     // can be placed at DBTest parent class
    /**
    * Convenience method for creating a book bundle
    *
    * @param int $count
    * @return mixed
    */
    protected function bundleFactory($bookCount = 2)
    {
        if ($bookCount <= 1) {
            throw new \RuntimeException('A bundle must have two or more books!');
        }
        
        $bundle = factory(\App\Bundle::class)->create();
        $books = $this->bookFactory($bookCount);
        $books->each( function ($book) use ($bundle) {
            $bundle->books()->attach($book);
        });
        
        return $bundle;
    }
    
    /**
    * Convenience method for creating a book with an author
    *
    * @param int $count
    * @return mixed
    */
    protected function bookFactory($count = 1)
    {
        $author = factory(\App\User::class)->create();
        $books = factory(\App\Book::class, $count)->make();
        // dd($books->first());
        if ($count === 1) {
            $book = $books->first();
            $book->author()->associate($author);
            $book->save();
            return $book;
        } else {
            $books->each(function ($book) use ($author) {
                $book->author()->associate($author);
                $book->save();
            });
        }
        return $books;
    }
}