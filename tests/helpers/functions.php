<?php
function create($class, $attribute=[], $n=null)
{
    return factory($class, $n)->create($attribute);
}

function make($class, $attribute=[], $n=null)
{
    return factory($class, $n)->make($attribute);
}
