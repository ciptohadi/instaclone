@component('mail::message')
# Reset Password

Mr/Mrs {{ $user->name }}, You have requested to reset your password. This is 
your temporary password {{ $tmp_password }} 

Yours sincerely,<br>
{{ env('APP_NAME') }}
@endcomponent
