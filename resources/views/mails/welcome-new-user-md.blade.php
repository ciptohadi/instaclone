@component('mail::message')
# Thank you for your registration

Welcome to instaclone.dev, Mr/Mrs {{ $user->name }}

@component('mail::button', ['url' => 'https://instaclone.dev'])
Browse photos
@endcomponent

@component('mail::panel')
    Some great quote for us
@endcomponent

Yours sincerely,<br>
{{ env('APP_NAME') }}
@endcomponent
