<?php

namespace App\Engines;

use TeamTNT\Scout\Engines\TNTSearchEngine;

class AppTNTSearchScoutEngine extends TNTSearchEngine
{
    /**
     * Update the given model in the index.
     *
     * @param Collection $models
     *
     * @return void
     */
    public function update($models)
    {
        $this->initIndex($models->first());
        $this->tnt->selectIndex("{$models->first()->searchableAs()}.index");
        $index = $this->tnt->getIndex();
        $index->setPrimaryKey($models->first()->getKeyName());

        $index->indexBeginTransaction();
        $models->each(function ($model) use ($index) {
            $array = $model->toSearchableArray();

            if (empty($array)) {
                return;
            }

            if ($model->getKey()) {
                /* nested array for eloquent relationship */
                /* convert nested array to string */
                array_walk($array, array($this, 'arrayToString'));
                
                $index->update($model->getKey(), $array);
            } else {
                $index->insert($array);
            }
        });
        $index->indexEndTransaction();
    }

    private function arrayToString(&$val, $key)
    {
        if (!is_string($val)) {
            $val = json_encode($val);
        }
    }
}
