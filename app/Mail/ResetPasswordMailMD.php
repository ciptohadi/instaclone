<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPasswordMailMD extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $tmp_password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, String $tmp_password)
    {
        $this->user = $user;
        $this->tmp_password = $tmp_password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mails.reset-password-md');
    }
}
