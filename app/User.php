<?php

namespace App;

use App\Traits\Followable;
use App\Traits\ModelTraits;
use Laravel\Scout\Searchable;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends BaseModel implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable, Followable, ModelTraits, Notifiable, Searchable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'gender', 'biography', 'avatar_path'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
   
    const RULES = [
            'name' =>  ['required', 'max:50'],
            'username' => ['required', 'min:6', 'max:25', 'unique:users,username'] ,
            'email' => ['required', 'email', 'max:50', 'unique:users,email'] ,
            'password' => ['required', 'min:6', 'max:25'],
            'gender' => ['required', 'regex:/^(male|female)$/i'],
            'biography' => ['nullable'],
        ];

    const RULE_ERROR_MESSAGES = [
                'gender.regex' => "Gender format is invalid: must equal 'male' or 'female'"
            
            ];

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }
    
    public function path()
    {
        return $this->host() . '/users/'. $this->id;
    }


    public function avatar_src()
    {
        return $this->host() . '/users/'. $this->id . '/avatar-src';
    }

    public function getJWTIdentifier()
    {
        return $this->getKey(); // Eloquent Model method
    }
    
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function is_the_auth_user()
    {
        return $this->is(app('auth')->user());
    }

    public function searchableAs()
    {
        return 'users_index';
    }
}
