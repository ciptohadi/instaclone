<?php
namespace App\Providers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use mmghv\LumenRouteBinding\RouteBindingServiceProvider as BaseServiceProvider;

class RouteBindingServiceProvider extends BaseServiceProvider
{
    /**
     * Boot the service provider
     */
    public function boot()
    {
        // The binder instance
        $binder = $this->binder;

        // Here we define our bindings
        $binder->implicitBind('App', '', '', '', function ($e) {
        
            // Or we can throw another exception for example :
            throw new ModelNotFoundException;
        });

        // Using a closure
        $binder->compositeBind(['photo', 'comment'], function ($photoKey, $commentKey) {
            $photo = \App\Photo::findOrFail($photoKey);
            $comment = $photo->comments()->find($commentKey);
            if (!$comment) {
                $comment = \App\Comment::where('photo_id', $photo->id)->findOrFail($commentKey);
            }
            return [$photo, $comment];
        }, function ($e) {
            // Or we can throw another exception for example :
            throw new ModelNotFoundException;
        });

        // Using a closure
        $binder->compositeBind(['photo', 'like'], function ($photoKey, $likeKey) {
            $photo = \App\Photo::findOrFail($photoKey);
            $like = $photo->likes()->findOrFail($likeKey);

            return [$photo, $like];
        }, function ($e) {
            // Or we can throw another exception for example :
            throw new ModelNotFoundException;
        });
    }
}
