<?php 
namespace App\Providers;

use Laravel\Scout\EngineManager;
use TeamTNT\TNTSearch\TNTSearch;
use TeamTNT\Scout\Console\ImportCommand;
use TeamTNT\Scout\TNTSearchScoutServiceProvider;
use App\Engines\AppTNTSearchScoutEngine;

class AppTNTSearchScoutServiceProvider extends TNTSearchScoutServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app[EngineManager::class]->extend('tntsearch', function ($app) {
            $tnt = new TNTSearch();
            
            $driver = config('database.default');
            $config = config('scout.tntsearch') + config("database.connections.{$driver}");

            $tnt->loadConfig($config);
            $tnt->setDatabaseHandle(app('db')->connection()->getPdo());
            
            $this->setFuzziness($tnt);
            $this->setAsYouType($tnt);

            return new AppTNTSearchScoutEngine($tnt);
        });

        if ($this->app->runningInConsole()) {
            $this->commands([
                ImportCommand::class,
            ]);
        }
    }
}
