<?php

namespace App\Providers;

use Berkayk\OneSignal\OneSignalServiceProvider;

class OneSignalLumenServiceProvider extends OneSignalServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (class_exists('Laravel\Lumen\Application')) {
            $this->app->configure('onesignal');
        }
    }
}
