<?php
namespace App\Transformers;

use App\Comment;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{
    //include
    protected $availableIncludes = [
        'commenter', 'child_comments'
    ];
    
    /**
    * Transform a Comment model into an array
    *
    * @param Comment $comment
    * @return array
    */
    public function transform(Comment $comment)
    {
        $grandparent_comment = $comment->grandparent_comment();
        
        $transformed_comment = [
            'id' => $comment->id,
            'content' => $comment->content,
            'path' => $comment->path(),
            'liked' => $comment->liked(),
            'likes' => [
                'count' => $comment->likes->count()
            ],
            'child_comments' =>[
                    'count' => count($comment->child_comments())
                ],
            'commenter' => [
                'id' => $comment->commenter->id,
                'name' => $comment->commenter->name,
                'username' => $comment->commenter->username,
                'avatar_src' => $comment->commenter->avatar_src(),
                'profile' => $comment->commenter->path()
               ],
            'commentable_obj' =>[
                    'type' => $comment->commentable_type,
                    'id' => $comment->commentable_id,
                    'photo_id' => $comment->photo_id,
                    'grandparent_comment_id' => $grandparent_comment ? $grandparent_comment->id : $grandparent_comment,
                    'owner' => [
                        'id' => $comment->getCommentableObj()['owner']->id,
                        'name' => $comment->getCommentableObj()['owner']->name,
                        'username' => $comment->getCommentableObj()['owner']->username,
                        'avatar_src' => $comment->getCommentableObj()['owner']->avatar_src(),
                        'profile' => $comment->getCommentableObj()['owner']->path()
                    ],
                ],
            'created' => $comment->created_at,
            'updated' => $comment->updated_at,
        ];
        
        return $transformed_comment;
    }
    
    public function includeCommenter(Comment $comment)
    {
        return $this->item($comment->commenter, new UserTransformer());
    }

    public function includeChildComments(Comment $comment)
    {
        return $this->collection(array_unique($comment->child_comments()), new CommentTransformer());
    }
}
