<?php
namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use Illuminate\Notifications\DatabaseNotification;

class NotificationTransformer extends TransformerAbstract
{
    /**
    * Transform a DatabaseNotification model into an array
    *
    * @param DatabaseNotification $notification
    * @return array
    */
    public function transform(DatabaseNotification $notification)
    {
        $transformed_notification = [
            'id' => $notification->id,
            'type' => $notification->type,
            'notifiable_id' => $notification->notifiable_id,
            'notifiable_type' => $notification->notifiable_type,
            'data' => $notification->data,
            "read_at" => $notification->read_at,
            "created_at" => $notification->created_at,
            "updated_at" => $notification->updated_at
        ];
        
        return $transformed_notification;
    }
}
