<?php
namespace App\Transformers;

use App\Photo;
use League\Fractal\TransformerAbstract;

class PhotoTransformer extends TransformerAbstract
{
    //include
    protected $availableIncludes = [
        'owner'
    ];
    
    /**
    * Transform a Book model into an array
    *
    * @param Photo $photo
    * @return array
    */
    public function transform(Photo $photo)
    {
        return [
            'id' => $photo->id,
            'description' => $photo->description,
            'path' => $photo->path(),
            'src' => $photo->src(),
            'liked' => $photo->liked(),
            'likes' => [
                    'count' => $photo->likes->count()
               ],
            'comments' =>[
                    'count' => $photo->comments->count(),
                    'total' => $this->getTotalComments($photo)
                ],
            'owner' => [
                'id' => $photo->owner->id,
                'name' => $photo->owner->name,
                'username' => $photo->owner->username,
                'avatar_src' => $photo->owner->avatar_src() . '?' . strtotime($photo->owner->updated_at),
                'profile' => $photo->owner->path(),
            ],
            'created' => $photo->created_at,
            'updated' => $photo->updated_at,
        ];
    }
    
    //add
    public function includeOwner(Photo $photo)
    {
        return $this->item($photo->owner, new UserTransformer());
    }

    private function getTotalComments($photo)
    {
        $totalComments = $photo->comments->count();

        foreach ($photo->comments as $comment) {
            $totalComments += count($comment->child_comments());
        }

        return $totalComments;
    }
}
