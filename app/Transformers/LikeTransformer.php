<?php
namespace App\Transformers;

use App\Like;
use App\Photo;
use League\Fractal\TransformerAbstract;

class LikeTransformer extends TransformerAbstract
{
    //include
    protected $availableIncludes = [
        'liker', 'likeable_obj'
    ];
    
    /**
    * Transform a Comment model into an array
    *
    * @param Comment $like
    * @return array
    */
    public function transform(Like $like)
    {
        $transformed_like = [
            'id' => $like->id,
            'liker' => [
                    'id' => $like->liker->id,
                    'name' => $like->liker->name,
                    'username' => $like->liker->username,
                    'profile' => $like->liker->path()
               ],
            'likeable_obj' =>[
                    'type' => $like->likeable_type,
                    'id' => $like->likeable_id
                ],
            'created' => $like->created_at,
            'updated' => $like->updated_at,
        ];
        
        return $transformed_like;
    }
    
    //add
    public function includeLiker(Like $like)
    {
        return $this->item($like->liker, new UserTransformer());
    }

    //add
    public function includeLikeableObj(Like $like)
    {
        $modelFull = '\\' . $like->likeable_type;
        $likeable_obj = $modelFull::findOrFail($like->likeable_id);
        $transformer = $like->likeable_type == Photo::class ? new PhotoTransformer() : new CommentTransformer();
        return $this->item($likeable_obj, $transformer);
    }
}
