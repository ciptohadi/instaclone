<?php
namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'photos'
    ];
    
    /**
    * Transform a User model into an array
    *
    * @param User $user
    * @return array
    */
    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'username' => $user->username,
            'email' => $user->email,
            'gender' => $user->gender,
            'path' => $user->path(),
            'avatar_src' => $user->avatar_src() . '?' . strtotime($user->updated_at),
            'biography' => $user->biography,
            'is_following' => $user->isFollowing(),
            'is_followed' => $user->isFollowed(),
            'followers' => ['count' => $user->followers()->count()],
            'followings' => ['count' => $user->followings()->count()],
            'photos' => ['count' => $user->photos()->count()],
            'created' => $user->created_at,
            'updated' => $user->updated_at,
        ];
    }

    public function includePhotos(User $user)
    {
        return $this->collection($user->photos, new PhotoTransformer());
    }
}
