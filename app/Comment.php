<?php

namespace App;

use App\User;
use App\Photo;
use App\Comment;
use App\Traits\Likeable;
use App\Traits\Commentable;
use Laravel\Scout\Searchable;

class Comment extends BaseModel
{
    use Likeable, Commentable, Searchable;

    private $child_comments_arr = [];

    protected $fillable = ['content', 'user_id', 'photo_id'];
    
    const RULES = [
        'content' => ['required'],
        'user_id' => ['required', 'exists:users,id'],
        'photo_id' => ['required', 'exists:photos,id']
    ];

    const RULE_ERROR_MESSAGES =  [];

    public function commentable()
    {
        return $this->morphTo();
    }

    public function delete()
    {
        try {
            $this->likes()->delete(); // DELETE * FROM files WHERE user_id = ? query
            // $this->delete_comments_recursively($this->comments);
            $this->delete_comments_recursively($this->comments);
    
            parent::delete();

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function path()
    {
        return '/photos/'. $this->photo_id . '/comments/' . $this->id;
    }

    public function getCommentableObj()
    {
        $commentable_obj = null;

        if ($this->commentable_type == __CLASS__) {
            $comment = Comment::findOrFail($this->commentable_id);
            $commentable_obj = [
                'owner' => $comment->commenter,
                'comment' => $comment
            ];
        } else {
            $photo = Photo::findOrFail($this->commentable_id);
            $commentable_obj = [
                'owner' => $photo->owner,
                'photo' => $photo
            ];
        }
        return $commentable_obj;
    }

    public function child_comments()
    {
        $comments = $this->comments;

        if ($comments->count() > 0) {
            foreach ($this->comments as $comment) {
                array_push($this->child_comments_arr, $comment);
                $this->get_comment_children($comment);
            }
            
            return $this->child_comments_arr;
        }
        return  $comments;
    }
      
    private function get_comment_children($comment)
    {
        $comments = $comment->comments;
        
        if ($comments->count() > 0) {
            foreach ($comments as $comment) {
                array_push($this->child_comments_arr, $comment);
                // this function calls itself, so its recursive.
                $this->get_comment_children($comment);
            }
        }
    }

    public function grandparent_comment()
    {
        if ($this->commentable_type == Photo::class) {
            return null;
        }

        $grandparent_comments = Comment::where('commentable_type', Photo::class)->get();
        foreach ($grandparent_comments as $grandparent_comment) {
            $child_comments = $grandparent_comment->child_comments();
            
            foreach ($child_comments as $child_comment) {
                if ($child_comment->id == $this->id) {
                    return $grandparent_comment;
                }
            }

            $comments = $grandparent_comment->comments;
            if ($comments->contains($this)) {
                return $grandparent_comment;
            }
        }
    }

    /**
     * Delete comments recursively (neseted comments of comments)
     *
     * @param [type] $comments
     * @return void
     */
    private function delete_comments_recursively($comments)
    {
        if ($comments->count() > 0) {
            foreach ($comments as $comment) {
                $comment->delete();
                $this->delete_comments_recursively($comment->comments);
            }
        }
    }

    public function searchableAs()
    {
        return 'comments_index';
    }
}
