<?php

namespace App\Traits;

use App\User;
use App\Comment;

/**
 * Trait to enable polymorphic ratings on a model.
 *
 * @package App
 */
trait Commentable
{
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
    
    public function commenter()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
