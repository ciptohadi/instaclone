<?php

namespace App\Traits;

use Exception;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

trait RespondWithJsonToExceptionTrait
{
    use RespondWithJsonTrait;

    /**
     * Creates a new JSON response based on exception type.
     *
     * @param Request $request
     * @param Exception $e
     * @return \Illuminate\Http\JsonResponse
     */
    protected function get_json_response_for_exception(Exception $e)
    {
        switch (true) {
            case $this->is_UnauthorizedException($e):
                $retval = $this->respond_with_json_to_UnauthorizedException($e);
                break;
            case $this->is_MethodNotAllowedHttpException($e):
                $retval = $this->respond_with_json_to_MethodNotAllowedHttpException($e);
                break;
            case $this->is_ModelNotFoundException($e):
                $retval = $this->respond_with_json_to_ModelNotFoundException($e);
                break;
            case $this->is_NotFoundHttpException($e):
                $retval = $this->respond_with_json_to_NotFoundHttpException($e);
                break;
            case $this->is_HttpException($e):
                $retval = $this->respond_with_json_to_HttpException($e);
                break;
            default:
                $retval = $this->respond_with_json_to_BadRequest($e, (string) $e->getMessage());
        }

        return $retval;
    }

    /**
     * Returns json response for generic bad request.
     *
     * @param string $message
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respond_with_json_to_BadRequest(Exception $e, $message = 'Bad request!')
    {
        return $this->respond_with_json_to_Exception($e, $message, Response::HTTP_BAD_REQUEST);
    }

    /**
     * Returns json response for method no allowed exception
     *
     * @param Exception $e
     * @param string $message
     * @return void
     */
    protected function respond_with_json_to_MethodNotAllowedHttpException(Exception $e, $message = 'Action is not allowed!')
    {
        return $this->respond_with_json_to_Exception($e, $message, Response::HTTP_METHOD_NOT_ALLOWED);
    }

    /**
     * Returns json response for common http exception
     *
     * @param Exception $e
     * @return void
     */
    protected function respond_with_json_to_HttpException(Exception $e)
    {
        return $this->respond_with_json_to_Exception($e);
    }

    /**
     * Returns json response for Eloquent model not found exception.
     *
     * @param string $message
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respond_with_json_to_ModelNotFoundException(Exception $e, $message='Record not found!')
    {
        return $this->respond_with_json_to_Exception($e, $message, Response::HTTP_NOT_FOUND);
    }

    /**
     * Returns json response for http not found exception
     *
     * @param Exception $e
     * @param string $message
     * @return void
     */
    protected function respond_with_json_to_NotFoundHttpException(Exception $e, $message='Route not found!')
    {
        return $this->respond_with_json_to_Exception($e, $message, Response::HTTP_NOT_FOUND);
    }

    /**
     * Returns json response for unauthorized exception
     *
     * @param Exception $e
     * @param string $message
     * @return void
     */
    protected function respond_with_json_to_UnauthorizedException(Exception $e, $message='Unauthorized!')
    {
        return $this->respond_with_json_to_Exception($e, $message, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Returns json response for common exception
     *
     * @param Exception $e
     * @param [type] $message
     * @param [type] $statusCode
     * @param array $headers
     * @return void
     */
    protected function respond_with_json_to_Exception(Exception $e = null, $message = null, $statusCode = null, $headers = [])
    {
        $payload = [
                'error' => [
                    'message' => $message ? $message : Response::$statusTexts[$e->getCode()],
                    'code'  => $statusCode ? $statusCode : $e->getCode()
                ]
            ];

        if ($this->isDebugMode() && ($e instanceof Exception) && !$this->is_UnauthorizedException($e)) {
            $payload['error']['debug'] = [
                                            'exception' => get_class($e),
                                            'trace' => $e->getTrace()
                                        ];
        }
        
        return $this->respond_with_json($payload, $statusCode, $headers);
    }

    /**
     * Returns json response for common error
     *
     * @param string $message
     * @param [type] $statusCode
     * @param array $headers
     * @return void
     */
    protected function respond_with_json_to_common_error($message = 'Something error!', $statusCode = Response::HTTP_BAD_REQUEST, $headers = [])
    {
        $payload = [
                'error' => [
                    'message' => $message,
                    'code'  => $statusCode
                ]
            ];

        return $this->respond_with_json($payload, $statusCode, $headers);
    }

    /**
     * Check if the given exception is a common http exception
     *
     * @param Exception $e
     * @return boolean
     */
    protected function is_HttpException(Exception $e)
    {
        return $e instanceof HttpException;
    }

    /**
     * Check if the given exception is an Eloquent model not found
     *
     * @param Exception $e
     * @return bool
     */
    protected function is_ModelNotFoundException(Exception $e)
    {
        return $e instanceof ModelNotFoundException;
    }

    /**
     * Check if the given exception is an not found http exception
     *
     * @param Exception $e
     * @return boolean
     */
    protected function is_NotFoundHttpException(Exception $e)
    {
        return $e instanceof NotFoundHttpException;
    }

    /**
     * Check if the given exception is an unauthorized exception
     *
     * @param Exception $e
     * @return boolean
     */
    protected function is_UnauthorizedException(Exception $e)
    {
        return $e instanceof UnauthorizedException;
    }

    /**
     * Check if the given exception is a method not allowed http exception
     *
     * @param Exception $e
     * @return boolean
     */
    protected function is_MethodNotAllowedHttpException(Exception $e)
    {
        return $e instanceof MethodNotAllowedHttpException;
    }
    
    /**
     * Check if it is in debug mode
     *
     * @return boolean
     */
    public function isDebugMode()
    {
        return (Boolean) env('APP_DEBUG');
    }
}
