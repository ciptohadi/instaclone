<?php

namespace App\Traits;

use Symfony\Component\HttpFoundation\Response;

trait RespondWithJsonTrait
{
    /**
     * Returns json response with message key
     *
     * @param string $content
     * @param array $data
     * @param [type] $statusCode
     * @param array $headers
     * @return void
     */
    protected function respond_with_json_message($content = 'Ok!', $data = [], $statusCode=Response::HTTP_OK, $headers = [])
    {
        $payload = [
                'message' => [
                    'content' => $content,
                    'code'  => $statusCode
                ]
            ];
        if (!empty($data)) {
            $payload += $data;
        }

        return $this->respond_with_json($payload, $statusCode, $headers);
    }

    /**
     * Returns json response.
     *
     * @param array|null $payload
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respond_with_json(array $payload=null, $statusCode=Response::HTTP_OK, $headers = [])
    {
        $payload = $payload ?: [];

        $payload= ['info' => config('app.info')] + $payload;

        return response()->json($payload, $statusCode, $headers);
    }
}
