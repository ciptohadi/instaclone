<?php
namespace App\Traits;

use App\User;

trait Followable
{

    /**
     * Follow the given user.
     *
     * @param User $user
     * @return mixed
     */
    public function follow(User $user)
    {
        if (! $this->isFollowingThis($user) && $this->id != $user->id) {
            return $this->followings()->attach($user);
        }
    }
    /**
     * Unfollow the given user.
     *
     * @param User $user
     * @return mixed
     */
    public function unFollow(User $user)
    {
        if ($this->isFollowingThis($user)) {
            return $this->followings()->detach($user);
        }
    }
    /**
     * Get all the users that this user is following.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followings()
    {
        return $this->belongsToMany(User::class, 'followers', 'follower_id', 'user_id');
    }
    /**
     * Get all the users that are following this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followers()
    {
        return $this->belongsToMany(User::class, 'followers', 'user_id', 'follower_id');
    }

    /**
     * Check if a given user is following this user.
     *
     * @param User $user
     * @return bool
     */

    public function isFollowingThis($user)
    {
        $check_following = null;

        if ($user && $user->id != $this->id) {
            $check_following = !! $this->followings()->where('user_id', $user->id)->count();
        }

        return  $check_following;
    }

    public function isFollowing()
    {
        return $this->isFollowingThis(app('auth')->user());
    }
    
    /**
     * Check if a given user is being followed by this user.
     *
     * @param User $user
     * @return bool
     */
    public function isFollowedBy($user)
    {
        $check_follower = null;

        if ($user && $user->id != $this->id) {
            $check_follower = !! $this->followers()->where('follower_id', $user->id)->count();
        }
        return $check_follower;
    }

    public function isFollowed()
    {
        return $this->isFollowedBy(app('auth')->user());
    }
}
