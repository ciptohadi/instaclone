<?php

namespace App\Traits;

use App\Like;

/**
 * Trait to enable polymorphic ratings on a model.
 *
 * @package App
 */
trait Likeable
{
    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    public function liked()
    {
        $authUser = app('auth')->user();
        $retVal = ['is' => null, 'id' => null];
        
        if ($authUser) {
            foreach ($this->likes as $like) {
                if ($like->user_id == $authUser->id) {
                    $retVal = ['is' => true, 'id' => $like->id];
                    break;
                }
            }
        }

        return $retVal;
    }
}
