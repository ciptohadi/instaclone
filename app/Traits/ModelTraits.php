<?php

namespace App\Traits;

/**
 * Trait to enable polymorphic ratings on a model.
 *
 * @package App
 */
trait ModelTraits
{
    public function host()
    {
        $protocol = 'http://';
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            $protocol = 'https://';
        }
            
        $host = $_SERVER['HTTP_HOST'];

        if (empty($host) && !empty($_SERVER['SERVER_NAME'])) {
            $host = $_SERVER['SERVER_NAME'];
        }
        // $uri = $_SERVER['REQUEST_URI'];
        // $protocol . $host . $uri;
        return $protocol . $host;
    }
}
