<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.u';

    public function getCreatedAtAttribute($date)
    {
        return $date;
    }
    
    public function getUpdatedAtAttribute($date)
    {
        return $date;
    }
}
