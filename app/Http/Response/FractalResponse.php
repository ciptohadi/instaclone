<?php
namespace App\Http\Response;

use Carbon\Carbon;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use League\Fractal\Resource\Item;
use App\Traits\RespondWithJsonTrait;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\ResourceInterface;
use League\Fractal\Serializer\SerializerAbstract;

class FractalResponse
{
    use RespondWithJsonTrait;
    
    /**
    * @var Manager
    */
    private $manager;
    
    /**
    * @var SerializerAbstract
    */
    private $serializer;
    
    private $request;
    
    public function __construct(Manager $manager, SerializerAbstract $serializer, Request $request)
    {
        $this->manager = $manager;
        $this->serializer = $serializer;
        $this->manager->setSerializer($serializer);
        $this->request = $request;
    }
    
    public function item($data, TransformerAbstract $transformer, $resourceKey = null)
    {
        return $this->createDataArray(new Item($data, $transformer, $resourceKey));
    }
    
    public function collection($data, TransformerAbstract $transformer, $resourceKey = null)
    {
        $now = Carbon::now()->format('Y-m-d H:i:s.u');
        $now64 = base64_encode($now);
        
        $limit = $this->request->input('limit', 3);
        $currentCursor64 = $this->request->input('cursor')? : $now64;

        $currentCursor = base64_decode($currentCursor64);
        $direction = $this->request->input('direction')? : 'older';

        if ($direction == 'newer') {
            $data = $data->where('created_at', '>', $currentCursor)->sortBy('created_at')->take($limit);
            $data = $data->sortByDesc('created_at');
        } else {
            $operand = $this->request->input('focus') == 'yes' ? '<=' : '<';
            $data = $data->where('created_at', $operand, $currentCursor)->take($limit);
        }

        if ($data->count() == 0) {
            if ($currentCursor == $now) {
                return $this->respond_with_json_message(
                                'No data exists'
                            );
            } else {
                if ($direction == 'newer') {
                    return $this->respond_with_json_message(
                            'Aready get latest data',
                            [ 'meta' => compact('direction') ]
                        );
                } else {
                    return $this->respond_with_json_message(
                        'Already get oldest data',
                        [ 'meta' => compact('direction') ]
                    );
                }
            }
        }

        $prevCursor64 = base64_encode($data->first()->created_at);
        $nextCursor64 = base64_encode($data->last()->created_at);
        
        $cursor = new Cursor($currentCursor64, $prevCursor64, $nextCursor64, $data->count());
        
        $resource = new Collection($data, $transformer, $resourceKey);
        $resource->setCursor($cursor);

        return $this->createDataArray($resource);
    }
    
    private function createDataArray(ResourceInterface $resource)
    {
        return $this->manager->createData($resource)->toArray();
    }
    
    /**
     * Get the includes from the request
     *
     * @param null $includes
     */
    public function parseIncludes($includes = null)
    {
        if (empty($includes)) {
            $includes = $this->request->query('include', '');
        }

        $this->manager->parseIncludes($includes);
    }
}
