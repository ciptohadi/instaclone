<?php

namespace App\Http\Controllers;

use App\User;
use App\Photo;

class FileController extends Controller
{
    /**
     * Returns image element for previewing image
     *
     * @param [type] $id
     * @return void
     */
    public function preview($id)
    {
        $first_path = $this->get_req_first_path();
        
        if ($first_path == 'users') {
            $img = '<img src="/users/' . $id . '/avatar-src">';
        } elseif ($first_path == 'photos') {
            $img = '<img src="/photos/' . $id . '/src">';
        } else {
            return null;
        }

        return $img;
    }

    /**
     * Returns image resource as value of src attr of img tag
     *
     * @param [type] $id
     * @return void
     */
    public function getImageSrc($id)
    {
        $first_path = $this->get_req_first_path();
       
        if ($first_path == 'users') {
            $file_path = User::findOrFail($id)->avatar_path;
            if (!$file_path) {
                $file_path = storage_path('uploads') . '/dummy-avatar.png';
            }
        } elseif ($first_path == 'photos') {
            $file_path = Photo::findOrFail($id)->file_path;
        } else {
            return null;
        }

        $handler = new \Symfony\Component\HttpFoundation\File\File($file_path);

        $lifetime = 180; // in seconds

        /**
        * Prepare some header variables
        */
        $file_time = $handler->getMTime(); // Get the last modified time for the file (Unix timestamp)

        $header_content_type = $handler->getMimeType();
        $header_content_length = $handler->getSize();
        $header_etag = md5($file_time . $file_path);
        $header_last_modified = gmdate('r', $file_time);
        $header_expires = gmdate('r', $file_time + $lifetime);

        $headers = array(
                'Content-Disposition' => 'inline; filename="' . basename($file_path) . '"',
                'Last-Modified' => $header_last_modified,
                'Cache-Control' => 'must-revalidate',
                'Expires' => $header_expires,
                'Pragma' => 'public',
                'Etag' => $header_etag
            );

        /**
        * Is the resource cached?
        */
        $h1 = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && $_SERVER['HTTP_IF_MODIFIED_SINCE'] == $header_last_modified;
        $h2 = isset($_SERVER['HTTP_IF_NONE_MATCH']) && str_replace('"', '', stripslashes($_SERVER['HTTP_IF_NONE_MATCH'])) == $header_etag;

        if ($h1 || $h2) {
            // File (image) is cached by the browser, so we don't have to send it again
            return response()->make('', 304, $headers);
        }
            

        $headers = array_merge($headers, array(
                'Content-Type' => $header_content_type,
                'Content-Length' => $header_content_length
            ));

        return response()->make(file_get_contents($file_path), 200, $headers);
    }
}
