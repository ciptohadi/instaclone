<?php

namespace App\Http\Controllers;

use OneSignal;
use App\Transformers\NotificationTransformer;

class NotificationController extends Controller
{
    /**
     * Return a collection of notifications
     *
     * @return void
     */
    public function index()
    {
        $auth_user = app('auth')->user();
        
        if ($auth_user->notifications->count()) {
            return $this->collection($auth_user->notifications, new NotificationTransformer(), 'notifications');
        }

        return $this->respond_with_json_message(
            'No notifications!'
        );
    }

    /**
     * Returns notification details
     *
     * @param [type] $id
     * @return void
     */
    public function show($id)
    {
        $auth_user = app('auth')->user();
        
        try {
            $notification = $auth_user->notifications->find($id);
            return $this->item($notification, new NotificationTransformer(), 'notification');
        } catch (\Exception $e) {
            return $this->respond_with_json_to_Error($e, $e->getMessage(), $e->getCode());
        }
    }

    /**
     * Mark a notification as read
     *
     * @param [type] $id
     * @return void
     */
    public function markAsRead($id)
    {
        $auth_user = app('auth')->user();
        
        try {
            $notification = $auth_user->notifications->find($id);
            $notification->markAsRead();
            $data = $this->item($notification, new NotificationTransformer(), 'notification');
            return $this->respond_with_json_message(
                'Successfully mark the notification as read!',
                $data
            );
        } catch (\Exception $e) {
            return $this->respond_with_json_to_Error($e, $e->getMessage(), $e->getCode());
        }
    }

    /**
     * Mark a notification as unread
     *
     * @param [type] $id
     * @return void
     */
    public function markAsUnread($id)
    {
        $auth_user = app('auth')->user();
        
        try {
            $notification = $auth_user->notifications->find($id);
            $notification->markAsUnread();
            $data = $this->item($notification, new NotificationTransformer(), 'notification');
            return $this->respond_with_json_message(
                'Successfully mark the notification as unread!',
                $data
            );
        } catch (\Exception $e) {
            return $this->respond_with_json_to_Error($e, $e->getMessage(), $e->getCode());
        }
    }
    
    public function sendWebPushNotification()
    {
        // return OneSignal::getNotifications(env('ONESIGNAL_APP_ID'), 3, 0);
        // return OneSignal::getPlayers(env('ONESIGNAL_APP_ID'), 3, 0) ;
    }
}
