<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Transformers\UserTransformer;
use App\Notifications\FollowNotification;
use Symfony\Component\HttpFoundation\Response;

class FollowController extends Controller
{
    /**
     * returns a collection of followers (users who follow us)
     *
     * @param Request $request
     * @param User $user
     * @return void
     */
    public function index_followers(Request $request, User $user)
    {
        $first_path = $this->get_req_first_path();

        if ($first_path == 'account') {
            $user = app('auth')->user();
        }

        if ($user->followers->count() < 1) {
            return $this->respond_with_json_message('No followers');
        }
        
        $followers = $this->latestOf($user->followers);

        return $this->collection($followers, new UserTransformer(), 'followers');
    }

    /**
     * returns a collection of followings (users which we follow)
     *
     * @param Request $request
     * @param User $user
     * @return void
     */
    public function index_followings(Request $request, User $user)
    {
        $first_path = $this->get_req_first_path();

        if ($first_path == 'account') {
            $user = app('auth')->user();
        }

        if ($user->followings->count() < 1) {
            return $this->respond_with_json_message('Not follow anyone');
        }

        $followings = $this->latestOf($user->followings);
        
        return $this->collection($followings, new UserTransformer(), 'followings');
    }
    
    /**
     * Follows a user
     *
     * @param User $user
     * @return void
     */
    public function store(User $user)
    {
        $auth_user = app('auth')->user();
        
        if ($user->is_the_auth_user()) {
            return $this->respond_with_json_to_common_error('Cannot follow ourselves');
        }
        
        if ($user->isFollowed()) {
            return $this->respond_with_json_to_common_error('You have followed this user');
        }

        try {
            $auth_user->follow($user);
            $follower_item = $this->item($auth_user, new UserTransformer(), 'follower');

            $data = [
                'data' => [
                    'followings' => [
                        'count' => $auth_user->followings->count()
                    ]
                ]
            ];
            
            $user->notify(new FollowNotification($follower_item['follower']));
        } catch (\Exception $e) {
            return $this->respond_with_json_to_Error($e, $e->getMessage(), $e->getCode());
        }
        
        return $this->respond_with_json_message(
            'Successfully follow ' . $user->username,
            $data,
            Response::HTTP_OK
        );
    }
    
    /**
     * Unfollows a user
     *
     * @param User $user
     * @return void
     */
    public function destroy(User $user)
    {
        $auth_user = app('auth')->user();

        if ($user->is_the_auth_user()) {
            return $this->respond_with_json_to_common_error('Cannot unfollow ourselves');
        }

        if (!$user->isFollowed()) {
            return $this->respond_with_json_to_common_error('You have unfollowed this user');
        }
        
        $auth_user->unFollow($user);
        
        $data = [
            'data' => [
                'followings' => [
                    'count' => $auth_user->followings->count()
                ]
            ]
        ];

        return $this->respond_with_json_message(
            'Successfully unfollow ' . $user->username,
            $data,
            Response::HTTP_OK
        );
    }
}
