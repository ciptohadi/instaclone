<?php

namespace App\Http\Controllers;

use App\Like;
use App\Photo;
use App\Comment;
use Illuminate\Http\Request;
use App\Transformers\LikeTransformer;
use App\Transformers\PhotoTransformer;
use App\Notifications\LikeNotification;
use App\Transformers\CommentTransformer;
use Symfony\Component\HttpFoundation\Response;

class LikeController extends Controller
{
    /**
     * Returns a Like collection of Photo and Comment
     *
     * @param Photo $photo
     * @param Comment $comment
     * @return void
     */
    public function index(Photo $photo, Comment $comment)
    {
        $likeable_obj = $this->get_likeable_obj($photo, $comment);
        
        $likes = $this->latestOf($likeable_obj->likes()->get());

        return $this->collection($likes, new LikeTransformer(), 'likes');
    }
    
    /**
     * Returns Like details
     *
     * @param Like $like
     * @return void
     */
    public function show(Like $like)
    {
        return $this->item($like, new LikeTransformer(), 'like');
    }
    
    /**
     * Likes a comment or a photo
     *
     * @param Request $request
     * @param Photo $photo
     * @param Comment $comment
     * @return void
     */
    public function store(Request $request, Photo $photo, Comment $comment)
    {
        $auth_user = app('auth')->user();
        
        $request['user_id'] = $auth_user->id;
        
        $request = $this->validate_request($request, Like::class, Like::RULES, Like::RULE_ERROR_MESSAGES);
       
        $likeable_obj = $this->get_likeable_obj($photo, $comment);
        
        if ($likeable_obj->liked()['is']) {
            return $this->respond_with_json_to_common_error('You have liked this');
        }

        try {
            $like =  $likeable_obj->likes()->create($request->all());
            $data = $this->item($like, new LikeTransformer(), 'like');
            
            if ($likeable_obj->owner) {
                $user = $likeable_obj->owner;
                $likeable_item = $this->item(Photo::findOrFail($likeable_obj->id), new PhotoTransformer());
            } else {
                $user = $likeable_obj->commenter;
                $comment = Comment::findOrFail($likeable_obj->id);
                $likeable_item = $this->item($comment, new CommentTransformer());
                
                if ($comment->commentable_type == Photo::class) {
                    $likeable_item['data']['commentable_obj'] = $this->item(Photo::findOrFail($comment->commentable_id), new PhotoTransformer(), 'photo');
                }
            }
            
            $user->notify(new LikeNotification([
                'like' => $data['like'],
                'likeable_obj' => $likeable_item['data']
            ]));
        } catch (\Exception $e) {
            return $this->respond_with_json_to_Error($e, $e->getMessage(), $e->getCode());
        }
        
        return $this->respond_with_json_message(
            'Successfully like an object!',
            $data,
            Response::HTTP_CREATED
        );
    }
    
    /**
     * Unlikes a photo or a comment
     *
     * @param Like $like
     * @return void
     */
    public function destroy(Like $like)
    {
        $this->is_authorized($like->liker);
        
        // true or false
        $result = $like->delete();
        
        if ($result) {
            $message_content = 'Successfully deleted';
            $statusCode = Response::HTTP_OK;

            return $this->respond_with_json_message($message_content, [], $statusCode);
        } else {
            $message_content = 'Failed delete';
            $statusCode = Response::HTTP_UNPROCESSABLE_ENTITY;

            return $this->respond_with_json_to_common_error($message_content, $statusCode);
        };
    }
    
    /**
     * Returns a likeable_obj
     *
     * @param Photo $photo
     * @param Comment $comment
     * @return void
     */
    private function get_likeable_obj(Photo $photo, Comment $comment)
    {
        $likeable_obj = ($comment->exists) ? $comment : $photo;
        
        return $likeable_obj;
    }
}
