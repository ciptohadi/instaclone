<?php

namespace App\Http\Controllers;

use App\Photo;
use App\Comment;
use Illuminate\Http\Request;
use App\Transformers\PhotoTransformer;
use App\Transformers\CommentTransformer;
use App\Notifications\CommentNotification;
use Symfony\Component\HttpFoundation\Response;

class CommentController extends Controller
{
    /**
     * Returns a collection of comments
     *
     * @param Request $request
     * @param Photo $photo
     * @param Comment $comment
     * @return void
     */
    public function index(Request $request, Photo $photo, Comment $comment)
    {
        $commentable_obj = $this->get_commentable_obj($photo, $comment);
       
        $path_segments = $this->get_req_path_segments();

        if (end($path_segments) == 'child-comments' || $comment->commentable_type == Comment::class) {
            $comments = collect($commentable_obj->child_comments());
        } else {
            $comments = $commentable_obj->comments()->get();
        }

        $comments = $this->latestOf($comments);
        
        return $this->collection($comments, new CommentTransformer(), 'comments');
    }
    
    /**
     * Show comment details
     *
     * @param Comment $comment
     * @return void
     */
    public function show(Comment $comment)
    {
        return $this->item($comment, new CommentTransformer(), 'comment');
    }
    
    /**
     * Creates a new comment
     *
     * @param Request $request
     * @param Photo $photo
     * @param Comment $comment
     * @return void
     */
    public function store(Request $request, Photo $photo, Comment $comment)
    {
        $auth_user = app('auth')->user();
        
        $request['user_id'] = $auth_user->id;
        $request['photo_id'] = $photo->id;
        
        $request = $this->validate_request($request, Comment::class, Comment::RULES, Comment::RULE_ERROR_MESSAGES);
        
        $commentable_obj = $this->get_commentable_obj($photo, $comment);

        try {
            $comment =  $commentable_obj->comments()->create($request->all());
            $data = $this->item($comment, new CommentTransformer(), 'comment');

            if ($commentable_obj->owner) {
                $user = $commentable_obj->owner;
                $commentable_item = $this->item($commentable_obj, new PhotoTransformer());
            } else {
                $user = $commentable_obj->commenter;
                $commentable_item = $this->item($commentable_obj, new CommentTransformer());
            }
            $user->notify(new CommentNotification([
                'comment' => $data['comment'],
                'commentable_obj' => $commentable_item['data']
            ]));
        } catch (\Exception $e) {
            return $this->respond_with_json_to_Error($e, $e->getMessage(), $e->getCode());
        }
        
        return $this->respond_with_json_message(
            'Successfully create a comment!',
            $data,
            Response::HTTP_OK
        );
    }
    
    /**
     * Updates comment
     *
     * @param Request $request
     * @param Photo $photo
     * @param Comment $comment
     * @return void
     */
    public function update(Request $request, Photo $photo, Comment $comment)
    {
        $this->is_authorized($comment->commenter);
        
        if (count($request->all())) {
            $auth_user = app('auth')->user();
            
            foreach ($request->all() as $key => $value) {
                if ($photo->$key == $value) {
                    unset($request[$key]);
                }
            }
            
            if (count($request->all())) {
                $request['user_id'] = $auth_user->id;
                $request['photo_id'] = $photo->id;
    
                $request = $this->validate_request($request, Comment::class, Comment::RULES, Comment::RULE_ERROR_MESSAGES, false);
                
                $comment->fill($request->all());
                $comment->save();
        
                $data = $this->item($comment, new CommentTransformer(), 'comment');
                $message_content = 'Successfully update comment content!';
                
                return $this->respond_with_json_message(
                    $message_content,
                    $data,
                    Response::HTTP_OK
                );
            }
        }
             
        $message = 'No update happened, no changed data';
        $statusCode = Response::HTTP_UNPROCESSABLE_ENTITY;
    
        return $this->respond_with_json_to_common_error($message, $statusCode);
    }
    
    /**
     * Deletes a comment
     *
     * @param Comment $comment
     * @return void
     */
    public function destroy(Comment $comment)
    {
        $this->is_authorized($comment->commenter);
        
        // true or false
        $result = $comment->delete();
        
        if ($result) {
            $message_content = 'Successfully deleted';
            $statusCode = Response::HTTP_OK;

            return $this->respond_with_json_message($message_content, [], $statusCode);
        } else {
            $message_content = 'Failed delete';
            $statusCode = Response::HTTP_UNPROCESSABLE_ENTITY;

            return $this->respond_with_json_to_common_error($message_content, $statusCode);
        };
    }

    /**
     * Get commentable object (Photo or Comment)
     *
     * @param Photo $photo
     * @param Comment $comment
     * @return void
     */
    private function get_commentable_obj(Photo $photo, Comment $comment)
    {
        $commentable_obj = ($comment->exists) ? $comment : $photo;
        
        return $commentable_obj;
    }
}
