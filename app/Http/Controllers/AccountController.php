<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Transformers\UserTransformer;
use App\Jobs\SendResetPasswordMailJob;
use Symfony\Component\HttpFoundation\Response;

class AccountController extends Controller
{
    /**
     * Login a user using email and password
     *
     * @param Request $request
     * @return void
     */
    public function login(Request $request)
    {
        $token = app('auth')->attempt($request->only('email', 'password'));

        if (!$token) {
            $message_content = 'Failed log you in! Check your email and password';
            $statusCode = Response::HTTP_UNPROCESSABLE_ENTITY;

            return $this->respond_with_json_to_common_error($message_content, $statusCode);
        } else {
            $message_content = 'Successfully log you in!';
            $statusCode = Response::HTTP_OK;
            
            return $this->respond_with_json_message(
                $message_content,
                compact('token'),
                $statusCode
            );
        }
    }

    /**
     * Logout a user
     *
     * @return void
     */
    public function logout()
    {
        $result = app('auth')->logout();
        
        if (is_null($result)) {
            $message_content = 'Successfully logout';
            $statusCode = Response::HTTP_OK;

            return $this->respond_with_json_message($message_content, [], $statusCode);
        } else {
            $message_content = 'Failed logout';
            $statusCode = Response::HTTP_UNPROCESSABLE_ENTITY;

            return $this->respond_with_json_to_common_error($message_content, $statusCode);
        }
    }

    /**
     * Reset a password
     *
     * @param Request $request
     * @return void
     */
    public function reset_password(Request $request)
    {
        $user = User::where('email', $request->only('email')['email'])->first();
        
        if (!$user) {
            return $this->respond_with_json_to_common_error('You are not registered', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $tmp_password = substr(md5(microtime()), rand(0, 26), 6);

        $user->password = Hash::make($tmp_password);

        dispatch(new SendResetPasswordMailJob($user, $tmp_password));

        return $this->respond_with_json_message('Check your email for your temporary password');
    }

    /**
     * Show auth user details
     *
     * @return void
     */
    public function show()
    {
        return $this->item(app('auth')->user(), new UserTransformer(), 'user');
    }

    /**
     * Update auth user avatar
     *
     * @param Request $request
     * @return void
     */
    public function update_avatar(Request $request)
    {
        $avatar_rules = User::RULES;
        
        foreach ($avatar_rules as $field => $rule_values) {
            if ($field != 'avatar') {
                unset($avatar_rules[$field]);
            }
        }

        $avatar_rules['avatar'] = ['required', 'image'];

        $request = $this->validate_request($request, User::class, $avatar_rules, User::RULE_ERROR_MESSAGES);

        
        if ($request->has('avatar')) {
            $user = app('auth')->user();
            
            $request = $this->upload_photo($request, $user);
            
            $user->fill($request->all());
            $user->save();

            $data = $this->item($user, new UserTransformer(), 'user');
            
            $message_content = 'Successfully update avatar!';
            
            return $this->respond_with_json_message(
                $message_content,
                $data
            );
        }

        $message_content = 'No avatar update happened, no changed data';
        $statusCode = Response::HTTP_UNPROCESSABLE_ENTITY;

        return $this->respond_with_json_to_common_error($message_content, $statusCode);
    }
    
    /**
     * Upload avatar photo
     *
     * @param Request $request
     * @param User $user
     * @return void
     */
    private function upload_photo(Request $request, User $user)
    {
        $file = $request->avatar;

        if ($file->isValid()) {
            $file_name = basename($file->path()) . '.'.  $file->extension();
        
            $destination_path = storage_path("uploads") . '/' .date('Y/m/d');

            $uploaded_file = $file->move($destination_path, $file_name);
            
            if (!is_null($user->avatar_path)) {
                unlink($user->avatar_path);
            }

            $request['avatar_path'] = $uploaded_file->getPathName();

            unset($request['avatar']);

            return $request;
        } else {
            $message_content = 'Upload file failed';
            $statusCode = Response::HTTP_UNPROCESSABLE_ENTITY;
    
            return $this->respond_with_json_to_common_error($message_content, $statusCode);
        }
    }
}
