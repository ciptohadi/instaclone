<?php

namespace App\Http\Controllers;

use App\User;
use App\Photo;
use Illuminate\Http\Request;
use App\Transformers\PhotoTransformer;
use Symfony\Component\HttpFoundation\Response;

class PhotoController extends Controller
{
    /**
     * Returns a collection of photos
     *
     * @param Request $request
     * @param User $user
     * @return void
     */
    public function index(Request $request, User $user)
    {
        $path_segments = $this->get_req_path_segments();
        $first_path = $path_segments[1];

        switch ($first_path) {
            case ($first_path == 'account'):
                $auth_user = app('auth')->user();
                $photos = collect();
                if (end($path_segments) == 'photos-timeline') {
                    foreach ($auth_user->followings as $user) {
                        $photos = $photos->merge($user->photos);
                    }
                }
                $photos = $photos->merge($auth_user->photos);
                break;
            case ($first_path == 'users'):
                $photos = $user->photos;
                break;
            default:
                $photos = Photo::get();
        }

        $photos = $this->latestOf($photos);
        
        // pagination with cursor
        return $this->collection($photos, new PhotoTransformer(), 'photos');
    }
    
    /**
     * Returns photo details
     *
     * @param Photo $photo
     * @return void
     */
    public function show(Photo $photo)
    {
        return $this->item($photo, new PhotoTransformer(), 'photo');
    }
    
    /**
     * Upload a photo file and description
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $auth_user = app('auth')->user();
        
        $request['user_id'] = $auth_user->id;
        
        $request = $this->validate_request($request, Photo::class, Photo::RULES, Photo::RULE_ERROR_MESSAGES);
        
        $request = $this->upload_photo($request);
        
        try {
            $photo = Photo::create($request->all());
            $data = $this->item($photo, new PhotoTransformer(), 'photo');
        } catch (\Exception $e) {
            return $this->respond_with_json_to_Error($e, $e->getMessage(), $e->getCode());
        }

        return $this->respond_with_json_message(
            'Successfully uploaded a photo!',
            $data,
            Response::HTTP_CREATED,
            ['Location' => route('photos.show', compact('photo'))]
        );
    }
    
    /**
     * Update a photo description
     *
     * @param Request $request
     * @param Photo $photo
     * @return void
     */
    public function update(Request $request, Photo $photo)
    {
        $this->is_authorized($photo->owner);

        if (count($request->all())) {
            $auth_user = app('auth')->user();
            
            $request['user_id'] = $auth_user->id;

            // not allowed update photo file, becaue like and commment will not be relevant
            $photo_rules = Photo::RULES;
            unset($photo_rules['file']);


            if ($request['description'] == $photo->description) {
                unset($request['description']);
            }

            if (count($request->all())) {
                $request = $this->validate_request($request, Photo::class, $photo_rules, Photo::RULE_ERROR_MESSAGES, false);
                
                $photo->fill($request->all());
                $photo->save();
        
                $data = $this->item($photo, new PhotoTransformer(), 'photo');
                
                $message_content = 'Successfully update photo description!';
                
                return $this->respond_with_json_message(
                    $message_content,
                    $data,
                    Response::HTTP_OK
                );
            }
        }
       
        $message_content = 'No update happened, no changed data';
        $statusCode = Response::HTTP_UNPROCESSABLE_ENTITY;

        return $this->respond_with_json_to_common_error($message_content, $statusCode);
    }
    
    /**
     * Deletes a photo
     *
     * @param Photo $photo
     * @return void
     */
    public function destroy(Photo $photo)
    {
        $this->is_authorized($photo->owner);
        
        // true or false
        $result = $photo->delete();
            
        if ($result) {
            $message_content = 'Successfully deleted';
            $statusCode = Response::HTTP_OK;
            $data = $this->item($photo, new PhotoTransformer(), 'photo');
            
            return $this->respond_with_json_message($message_content, $data, $statusCode);
        } else {
            $message_content = 'Failed delete';
            $statusCode = Response::HTTP_UNPROCESSABLE_ENTITY;

            return $this->respond_with_json_to_common_error($message_content, $statusCode);
        }
    }

    /**
     * Upload a photo file
     *
     * @param Request $request
     * @param boolean $update
     * @return void
     */
    private function upload_photo(Request $request, $update = false)
    {
        $file = $request->file;

        if ($file->isValid()) {
            $file_name = basename($file->path()) . '.'.  $file->extension();
        
            $destination_path = storage_path("uploads") . '/' .date('Y/m/d');

            $uploaded_file  = $file->move($destination_path, $file_name);

            if ($update) {
                unlink($photo->file_path);
            }

            $request['file_path'] = $uploaded_file->getPathName();
            
            unset($request['file']);

            return $request;
        } else {
            $message = 'Upload file failed';
            $statusCode = Response::HTTP_UNPROCESSABLE_ENTITY;
    
            $payload = [
                'error' => [
                    'message' => $message,
                    'code'  => $statusCode
                ]
            ];
    
            return $this->respond_with_json($payload, $statusCode);
        }
    }
}
