<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Traits\RespondWithJsonToExceptionTrait;
use App\Http\Response\FractalResponse;
use League\Fractal\TransformerAbstract;
use Illuminate\Validation\UnauthorizedException;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use RespondWithJsonToExceptionTrait;
    /**
    * @var FractalResponse
    */
    private $fractal;
    
    public function __construct(FractalResponse $fractal)
    {
        $this->fractal = $fractal;
        $this->fractal->parseIncludes();
        $this->set_custom_validation_error_format();
    }
    
    /**
    * @param $data
    * @param TransformerAbstract $transformer
    * @param null $resourceKey
    * @return array
    */
    public function item($data, TransformerAbstract $transformer, $resourceKey = null)
    {
        return $this->fractal->item($data, $transformer, $resourceKey);
    }
    
    /**
    * @param $data
    * @param TransformerAbstract $transformer
    * @param null $resourceKey
    * @return array
    */
    public function collection($data, TransformerAbstract $transformer, $resourceKey = null)
    {
        return $this->fractal->collection($data, $transformer, $resourceKey);
    }
    

    /**
     * Custom error response format for validation errors
     *
     * @return Illuminate\Http\Response
     */
    private function set_custom_validation_error_format()
    {
        static::$errorFormatter = function ($validator) {
            $arr = [];
            foreach ($validator->errors()->toArray() as $key => $value) {
                $arr[$key] = $value[0];
            }
 
            return [
                'info' => config('app.info'),
                'errors' => $arr,
                'meta'   => [
                    'code'    => 422,
                    'message' => 'VALIDATION_FAILED',
                ],
            ];
        };
    }

    /**
     * Validates request
     *
     * @param Request $request
     * @param string $model_class
     * @param array $rules
     * @param array $rule_error_messages
     * @param boolean $required
     * @return void
     */
    protected function validate_request(Request $request, $model_class = '', $rules = [], $rule_error_messages = [], $required = true)
    {
        $model = '\\' . $model_class;
        $accepted_request_arr = $request->only(array_keys($model::RULES));
        $request = $this->sanitize_request($request, $accepted_request_arr);
       
        if (!$required) {
            $new_rules = [];
            
            foreach ($rules as $field => $rule_values) {
                if (($key = array_search('required', $rule_values)) !== false) {
                    unset($rule_values[$key]);
                }
                $new_rules[$field] = $rule_values;
            }

            $rules = $new_rules;
        }
        
        $this->validate($request, $rules, $rule_error_messages);

        if ($model_class == User::class && $request->has('password')) {
            $request['password'] = Hash::make($request['password']);
        }

        return $request;
    }

    /**
     * Sanitize request by only receive accepted input
     *
     * @param Request $request
     * @param array $accepted_request_arr
     * @return void
     */
    private function sanitize_request(Request $request, array $accepted_request_arr)
    {
        foreach (array_diff($request->all(), $accepted_request_arr) as $key => $value) {
            unset($request[$key]);
        }

        return $request;
    }

    /**
     * Gets first path of request
     *
     * @return void
     */
    protected function get_req_first_path()
    {
        return $this->get_req_path_segments()[1];
    }

    /**
     * Gets request path segments
     *
     * @return void
     */
    protected function get_req_path_segments()
    {
        return explode('/', parse_url($_SERVER['REQUEST_URI'])['path']);
    }

    /**
     * Sorts collection by latest
     *
     * @param [type] $collection
     * @param string $key
     * @return void
     */
    protected function latestOf($collection, $key = 'created_at')
    {
        return $collection->sortByDesc($key);
    }

    /**
     * Checks if current user is the auth user
     *
     * @param User $user
     * @return boolean
     */
    protected function is_authorized(User $user)
    {
        if (!$user->is_the_auth_user()) {
            throw new UnauthorizedException;
        }
    }
}
