<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Transformers\UserTransformer;
use App\Jobs\SendWelcomeNewUserMailJob;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    /**
     * Returns a collection of users
     *
     * @param Request $request
     * @return void
     */
    public function index(Request $request)
    {
        $users = $this->latestOf(User::get());

        return $this->collection($users, new UserTransformer(), 'users');
    }
    
    /**
     * Returns a user details
     *
     * @param User $user
     * @return void
     */
    public function show(User $user)
    {
        return $this->item($user, new UserTransformer(), 'user');
    }
    
    /**
     * Creates a new user
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $request = $this->validate_request($request, User::class, $this->get_user_rules(), User::RULE_ERROR_MESSAGES);

        try {
            $user = User::create($request->all());
           
            $data = $this->item($user, new UserTransformer(), 'user');

            if ($request->is('account/register') && $user instanceof User) {
                dispatch(new SendWelcomeNewUserMailJob($user));
            }
        } catch (\Exception $e) {
            return $this->respond_with_json_to_Exception($e, $e->getMessage(), $e->getCode());
        }

        return $this->respond_with_json_message(
                'Successfully created a new user. You can check your email for welcome message',
                $data,
                Response::HTTP_CREATED,
                ['Location' => route('users.show', compact('user'))]
            );
    }
    
    /**
     * Update a user details
     *
     * @param Request $request
     * @param User $user
     * @return void
     */
    public function update(Request $request, User $user)
    {
        $path_segments = $this->get_req_path_segments();
        $first_path = $path_segments[1];

        if ($first_path == 'account') {
            $user = app('auth')->user();
        }

        $this->is_authorized($user);

        if (count($request->all())) {
            if ($this->get_req_first_path() == 'account') {
                $user = app('auth')->user();
            }

            foreach ($request->all() as $key => $value) {
                if ($key == 'avatar') {
                    continue;
                } elseif ($key == 'password') {
                    if (empty(trim($request[$key])) || Hash::check($request[$key], $user->$key)) {
                        unset($request[$key]);
                        continue;
                    }
                }
                
                if ($user->$key == $value) {
                    unset($request[$key]);
                }
            }
           
            if (count($request->all())) {
                $request = $this->validate_request($request, User::class, $this->get_user_rules(), User::RULE_ERROR_MESSAGES, false);
           
                $user->fill($request->all());
                $user->save();

                $data = $this->item($user, new UserTransformer(), 'user');
                
                $fields_arr = array_keys($request->all());
                $fields_arr_single = (count($fields_arr) == 1);
                $fields_str = '';

                foreach ($fields_arr as $field) {
                    $fields_str .= $fields_arr_single ? " $field" :
                                        (($field == end($fields_arr)) ? " and $field" :
                                            (($field == $fields_arr[0]) ? " $field" : ", $field"));
                }

                $message_content = 'Successfully update' . $fields_str . '!';
                
                return $this->respond_with_json_message(
                    $message_content,
                    $data,
                    Response::HTTP_OK
                );
            }
        }
        
        $message_content = 'No update happened, no changed data';
        $statusCode = Response::HTTP_UNPROCESSABLE_ENTITY;

        return $this->respond_with_json_to_common_error($message_content, $statusCode);
    }
    
    /**
     * Deletes a user
     *
     * @param User $user
     * @return void
     */
    public function destroy(User $user)
    {
        $this->is_authorized($user);

        // true or false
        $result = $user->delete();
        
        if ($result) {
            $message_content = 'Successfully deleted';
            $statusCode = Response::HTTP_OK;
            $data = $this->item($user, new UserTransformer(), 'user');

            return $this->respond_with_json_message($message_content, $data, $statusCode);
        } else {
            $message_content = 'Failed delete';
            $statusCode = Response::HTTP_UNPROCESSABLE_ENTITY;

            return $this->respond_with_json_to_common_error($message_content, $statusCode);
        }
    }

    /**
     * Get user rules
     *
     * @return void
     */
    private function get_user_rules()
    {
        $user_rules = User::RULES;
        
        unset($user_rules['avatar']);

        return $user_rules;
    }
}
