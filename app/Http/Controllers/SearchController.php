<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transformers\UserTransformer;
use App\Transformers\PhotoTransformer;
use App\Transformers\CommentTransformer;

class SearchController extends Controller
{
    /**
     * Return search results
     *
     * @param Request $request
     * @return void
     */
    public function index(Request $request)
    {
        $model = $request->query('model');
        $modelFull = '\\App\\' . $model;
        $key = $request->query('query');
        $searchResults = $modelFull::search($key)->get();
        $searchResults = $this->latestOf($searchResults);
        switch ($model) {
            case 'User':
                $searchResults = $this->collection($searchResults, new UserTransformer(), 'users');
                break;
            case 'Photo':
                $searchResults = $this->collection($searchResults, new PhotoTransformer(), 'photos');
                break;
            
            case 'Comment':
                $searchResults = $this->collection($searchResults, new CommentTransformer(), 'comments');
                break;
        }
        return $searchResults;
    }
}
