<?php

namespace App\Jobs;

use App\User;
use App\Mail\ResetPasswordMailMD;
use Illuminate\Support\Facades\Mail;

class SendResetPasswordMailJob extends Job
{
    protected $user;
    protected $tmp_password;

    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, String $tmp_password)
    {
        $this->user = $user;
        $this->tmp_password = $tmp_password;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->user)->send(new ResetPasswordMailMD($this->user, $this->tmp_password));
    }
}
