<?php

namespace App\Jobs;

use App\User;
use App\Mail\WelcomeNewUserMailMD;
use Illuminate\Support\Facades\Mail;

class SendWelcomeNewUserMailJob extends Job
{
    protected $user;

    public $tries = 10;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->user)->send(new WelcomeNewUserMailMD($this->user));
    }
}
