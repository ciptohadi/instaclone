<?php

namespace App;

use App\Traits\Likeable;
use App\Traits\Commentable;
use App\Traits\ModelTraits;
use Laravel\Scout\Searchable;

class Photo extends BaseModel
{
    use Likeable, Commentable, ModelTraits, Searchable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description', 'user_id', 'file_path'];

    const RULES = [
        'description' => ['required'],
        'file' => ['required', 'image'],
        'user_id' => ['required', 'exists:users,id']
    ];

    const RULE_ERROR_MESSAGES =  [];
    
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function path()
    {
        return $this->host() . '/photos/'. $this->id;
    }

    public function src()
    {
        return $this->host() . '/photos/'. $this->id . '/src';
    }

    public function delete()
    {
        try {
            $this->likes()->delete(); // DELETE * FROM files WHERE user_id = ? query
            $this->comments()->delete();
            parent::delete();
            
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function searchableAs()
    {
        return 'photos_index';
    }
}
