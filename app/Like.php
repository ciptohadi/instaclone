<?php

namespace App;

class Like extends BaseModel
{
    protected $fillable = ['user_id'];

    const RULES = [
        'user_id' => ['required', 'exists:users,id']
    ];

    const RULE_ERROR_MESSAGES =  [];

    public function likeable()
    {
        return $this->morphTo();
    }


    public function liker()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
